<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 06.08.2019
  Time: 22:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="pagecontent" prefix="adminpage.">

    <c:if test="${sessionScope.admin == null}">
        <jsp:forward page="../../../index.jsp"/>
    </c:if>
<html lang="${sessionScope.locale}">
<head>
    <title>Title</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../../../resources/styles/admin_tab.css" type="text/css">
</head>
<body>
<table>
    <thead>
    <tr>
        <th><fmt:message key="cliId"/></th>
        <th><fmt:message key="cliname"/></th>
        <th><fmt:message key="cliphone"/></th>
        <th><fmt:message key="climail"/></th>
        <th><fmt:message key="clipass"/></th>
        <th><fmt:message key="clidate"/></th>
        <th><fmt:message key="clidisc"/></th>
        <th><fmt:message key="clistatus"/></th>
        <th><fmt:message key="cliblock"/></th>
    </tr>
    </thead>

    <tbody>
    <form id="user_form" action="controller" method="post" accept-charset="utf-8">
        <c:forEach var="user" items = "${requestScope.adminInfo}">
            <tr>
                <td>${user.id}</td>
                <td>${user.name}</td>
                <td>${user.phone}</td>
                <td>${user.email}</td>
                <td>${user.password}</td>
                <td>${user.orderDate}</td>
                <td>${user.discount}</td>
                <td>${user.status}</td>
                <td><input name="block_id[]" type="text" value="${user.banReason}"></td>
                <input type="hidden" name="users_id[]" value="${user.id}"/>
            </tr>
        </c:forEach>
        <input type="hidden" name="form" value="user_form"/>
    </form>

    </tbody>

</table>
<input class="upd_btn" form="user_form" type="submit" value="<fmt:message key="cliupd"/>">
</body>
</html>
</fmt:bundle>