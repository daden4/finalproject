<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 06.08.2019
  Time: 15:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="pagecontent" prefix="adminpage.">

    <c:if test="${sessionScope.admin == null}">
        <jsp:forward page="../../../index.jsp"/>
    </c:if>
<!DOCTYPE html>
<html lang="${sessionScope.locale}">
<head>
    <meta charset="UTF-8">
    <title>admin</title>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" href="../../../resources/styles/admin.css" type="text/css">
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="../../../resources/scripts/ajax.js" type="text/javascript"></script>
    <script src="../../../resources/scripts/validator.js" type="text/javascript"></script>
</head>

<body>

<div class="main-overlay"></div>
<div class="bg-image">
    <div class="header-back"></div>
</div>

<header>

    <div class="logo">
        <img src="../../../resources/images/logo.png" alt="Логотип">
    </div>

    <div class="show-menu-btn">
        <i class="fa fa-bars"></i>
    </div>

    <div id = "menu-scroll" class="menu visible-lg">
        <ul>
            <li><span class="login"></span></li>
            <li><span><fmt:message key="adminphone"/> <span class="current-phone"></span></span></li>
            <li><span><fmt:message key="adminmail"/> <span class="current-mail"></span></span></li>
        </ul>
    </div>

</header>
<div class="container">

    <form id="admin_form" action="controller" method="post">
        <input type="submit" value="<fmt:message key="selprod"/>" name="product_sel_btn" class="change-btn">
        <input type="submit" value="<fmt:message key="seluser"/>" name="client_sel_btn" class="change-btn" >
        <input type="submit" value="<fmt:message key="selorder"/>" name="order_sel_btn" class="change-btn" >
        <input type="submit" value="<fmt:message key="selblock"/>" name="reason_sel_btn" class="change-btn" >
        <input type="hidden" name="form" value="admin_form" class="change-form"/>
    </form>


    <c:set var = "button" scope="request" value="${requestScope.buttonType}"/>
    <c:choose>
        <c:when test="${button eq 'product'}">
            <c:import url="admin_products.jsp" />
        </c:when>

        <c:when test="${button eq 'client' }">
            <c:import url="admin_clients.jsp" />
        </c:when>

        <c:when test="${button eq 'order'}">
            <c:import url="admin_orders.jsp" />
        </c:when>

        <c:when test="${button eq 'blockreason'}">
            <c:import url="admin_reason.jsp" />
        </c:when>

        <c:otherwise>
        </c:otherwise>
    </c:choose>

    <form id="back_form" action="controller" method="post" novalidate="" accept-charset="utf-8">
        <input type="submit" value="<fmt:message key="btnback"/>" id="back-btn">
        <input type="hidden" name="form" value="back"/>
    </form>

</div>
<script>
    $(function () {

        <c:set var="user" value="${sessionScope.admin}"/>
        $(".login").text('${user.name}');
        $(".current-phone").text('${user.phone}');
        $(".current-mail").text('${user.email}');
    });
</script>
</body>
</html>
</fmt:bundle>