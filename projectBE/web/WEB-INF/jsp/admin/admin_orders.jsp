<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 06.08.2019
  Time: 22:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="pagecontent" prefix="adminpage.">

    <c:if test="${sessionScope.admin == null}">
        <jsp:forward page="../../../index.jsp"/>
    </c:if>
<html lang="${sessionScope.locale}">
<head>
    <title>Title</title>
    <link rel="stylesheet" href="../../../resources/styles/admin_tab.css" type="text/css">
</head>
<body>

<table>
    <thead>
    <tr>
        <th><fmt:message key="ordid"/></th>
        <th><fmt:message key="ordprice"/></th>
        <th><fmt:message key="ordname"/></th>
        <th><fmt:message key="orddata"/></th>
        <th><fmt:message key="ordpizza"/></th>
        <th><fmt:message key="ordqua"/></th>
        <th><fmt:message key="ordsalad"/></th>
        <th><fmt:message key="ordqua"/></th>
        <th><fmt:message key="orddrink"/></th>
        <th><fmt:message key="ordqua"/></th>
    </tr>
    </thead>

    <tbody>
        <c:forEach var="order" items = "${requestScope.adminInfo}">
            <tr>
            <td>${order.orderId}</td>
            <td>${order.orderPrice}</td>
            <td>${order.client.name}</td>
            <td>${order.orderDate}</td>
            <c:set var="counter" value="${-1}"/>
            <c:forEach var="product" items="${order.productList}">
                <c:choose>
                    <c:when test="${sessionScope.locale eq 'ru'}">
                        <c:set var="productName" value="${product.productName}"/>
                    </c:when>
                    <c:when test="${sessionScope.locale eq 'en'}">
                        <c:set var="productName" value="${product.enName}"/>
                    </c:when>
                </c:choose>
                <c:if test="${product.productType eq 'PIZZA'}">
                    <c:set var="counter" value="${counter + 1}"/>
                </c:if>
                <c:if test="${counter > 0}">
                    <tr></tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <c:set var="counter" value="${-1}"/>
                </c:if>
                <td>${productName}</td>
                <td>${product.quantity}</td>
            </c:forEach>
            </tr>
        </c:forEach>
    </tbody>
</table>


</body>
</html>
</fmt:bundle>