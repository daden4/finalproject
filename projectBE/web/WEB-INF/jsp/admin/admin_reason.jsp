<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 06.08.2019
  Time: 23:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="pagecontent" prefix="adminpage.">

    <c:if test="${sessionScope.admin == null}">
        <jsp:forward page="../../../index.jsp"/>
    </c:if>
<html lang="${sessionScope.locale}">
<head>
    <title>Title</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../../../resources/styles/admin_tab.css" type="text/css">
</head>
<body>
<table>
    <thead>
    <tr>
        <th><fmt:message key="reasid"/></th>
        <th><fmt:message key="reasname"/></th>
        <th><fmt:message key="reasdesc"/></th>
    </tr>
    </thead>

    <tbody>
    <c:forEach var="blockReason" items = "${requestScope.adminInfo}">
        <tr>
            <td>${blockReason.id}</td>
            <td>${blockReason.name}</td>
            <td>${blockReason.description}</td>
            <c:set var="lastReasonId" value="${blockReason.id}"/>
        </tr>
    </c:forEach>

    <form id="block_form" action="controller" method="post" accept-charset="utf-8">
        <td>${lastReasonId + 1}</td>
        <td><input name="reason_name" type="text"></td>
        <td><input name="reason_description" type="text"></td>
        <input type="hidden" name="form" value="block_form"/>
    </form>
    </tbody>

</table>
<input class="save_btn" form="block_form" type="submit" value="<fmt:message key="reasadd"/>">
</body>
</html>
</fmt:bundle>