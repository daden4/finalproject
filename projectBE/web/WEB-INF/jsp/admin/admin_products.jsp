<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 06.08.2019
  Time: 22:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="pagecontent" prefix="adminpage.">

    <c:if test="${sessionScope.admin == null}">
        <jsp:forward page="../../../index.jsp"/>
    </c:if>
<html lang="${sessionScope.locale}">
<head>
    <title>Title</title>
    <link rel="stylesheet" href="../../../resources/styles/admin_tab.css" type="text/css">
</head>
<body>
<div class="product-table">

    <table>
        <thead>
        <tr>
            <th><fmt:message key="prodtype"/></th>
            <th><fmt:message key="prodid"/></th>
            <th><fmt:message key="prodname"/></th>
            <th><fmt:message key="prodprice"/></th>
            <th><fmt:message key="pizzaprodsize"/></th>

        </tr>
        </thead>
        <tbody>
        <c:forEach var="product" items = "${requestScope.adminInfo}">
            <c:choose>
                <c:when test="${sessionScope.locale eq 'ru'}">
                    <c:set var="productName" value="${product.productName}"/>
                </c:when>
                <c:when test="${sessionScope.locale eq 'en'}">
                    <c:set var="productName" value="${product.enName}"/>
                </c:when>
            </c:choose>

            <c:if test="${product.productType eq 'PIZZA'}">
                <tr>
                    <td>${product.productType}</td>
                    <td>${product.productID}</td>
                    <td>${productName}</td>
                    <td>${product.price}</td>
                    <td>${product.size}</td>
                </tr>
            </c:if>
        </c:forEach>
        </tbody>
    </table>

    <div class="right-table">

        <table>
            <thead>
            <tr>
                <th><fmt:message key="prodtype"/></th>
                <th><fmt:message key="prodid"/></th>
                <th><fmt:message key="prodname"/></th>
                <th><fmt:message key="prodprice"/></th>
                <th><fmt:message key="drinkprodsize"/></th>

            </tr>
            </thead>
            <tbody>
            <c:forEach var="product" items = "${requestScope.adminInfo}">
                <c:choose>
                    <c:when test="${sessionScope.locale eq 'ru'}">
                        <c:set var="productName" value="${product.productName}"/>
                    </c:when>
                    <c:when test="${sessionScope.locale eq 'en'}">
                        <c:set var="productName" value="${product.enName}"/>
                    </c:when>
                </c:choose>

                <c:if test="${product.productType eq 'DRINK'}">
                    <tr>
                        <td>${product.productType}</td>
                        <td>${product.productID}</td>
                        <td>${productName}</td>
                        <td>${product.price}</td>
                        <td>${product.size}</td>
                    </tr>
                </c:if>
            </c:forEach>
            </tbody>
        </table>

        <table>
            <thead>
            <tr>
                <th><fmt:message key="prodtype"/></th>
                <th><fmt:message key="prodid"/></th>
                <th><fmt:message key="prodname"/></th>
                <th><fmt:message key="prodprice"/></th>
                <th><fmt:message key="saladprodsize"/></th>

            </tr>
            </thead>
            <tbody>
            <c:forEach var="product" items = "${requestScope.adminInfo}">
                <c:choose>
                    <c:when test="${sessionScope.locale eq 'ru'}">
                        <c:set var="productName" value="${product.productName}"/>
                    </c:when>
                    <c:when test="${sessionScope.locale eq 'en'}">
                        <c:set var="productName" value="${product.enName}"/>
                    </c:when>
                </c:choose>

                <c:if test="${product.productType eq 'SALAD'}">
                    <tr>
                        <td>${product.productType}</td>
                        <td>${product.productID}</td>
                        <td>${productName}</td>
                        <td>${product.price}</td>
                        <td>${product.size}</td>
                    </tr>
                </c:if>
            </c:forEach>
            </tbody>
        </table>

    </div>

</div>

</body>
</html>
</fmt:bundle>
