<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 24.07.2019
  Time: 21:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="pagecontent" prefix="mainpage.">
  <!DOCTYPE html>
  <html lang="${sessionScope.locale}">
  <head>
    <meta charset="UTF-8">
    <title>Pizzeria</title>
    <link rel="stylesheet" type="text/css" href="resources/styles/task_style.css">
    <link rel="stylesheet" href="resources/styles/media.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="resources/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="resources/slick/slick-theme.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="resources/font-awesome/css/font-awesome.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="resources/scripts/main.js" type="text/javascript"></script>
    <script src="resources/scripts/ajax.js" type="text/javascript"></script>
    <script src="resources/scripts/pizza_price.js" type="text/javascript"></script>
    <script src="resources/scripts/jsLoc.js" type="text/javascript"></script>
    <script src="resources/scripts/slider.js" type="text/javascript"></script>
    <script src="resources/scripts/validator.js" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script type="text/javascript" src="resources/slick/slick.min.js"></script>
  </head>

  <body>

  <c:set var="locale" value="${sessionScope.locale}"/>
  <c:if test="${sessionScope.locale == null}">
    <c:set var="locale" value="ru"/>
  </c:if>

  <span id="pageLanguage" hidden>${locale}</span>
  <ul class="m-menu">
    <button class="registration menu-link"><fmt:message key="login"/></button>
    <a href="#menu-scroll" class="start-btn menu-link"><fmt:message key="start"/></a>
    <a href="#pizza-carousel" class="menu-link"><fmt:message key="pizzahead"/></a>
    <a href="#salad-carousel" class="menu-link"><fmt:message key="saladhead"/></a>
    <a href="#drink-carousel" class="menu-link"><fmt:message key="drinkhead"/></a>
    <span class="phone menu-link"><fmt:message key="phone"/><br></span>
    <span class="phone menu-link">+375 25 133-73-22</span>
    <div class="hide-menu-btn">
      <i class="fa fa-times"></i>
    </div>
  </ul>

  <ul class="user_menu modal">
    <a href="account.jsp" class="menu-link"><fmt:message key="usercabinet"/></a>
    <hr>
    <div class="menu-link">
      <form id="back_form" action="controller" method="post" novalidate="" accept-charset="utf-8">
        <input type="submit" value="<fmt:message key="userexit"/>" id="back-btn">
        <input type="hidden" name="form" value="back"/>
      </form>
    </div>
  </ul>

  <div class="main">

    <div class="main-overlay"></div>
    <div class="bg-image">
      <div class="header-back"></div>
    </div>

    <header>

      <div class="logo">
        <img src="resources/images/logo.png" alt="Логотип">
      </div>



      <div class="show-menu-btn">
        <i class="fa fa-bars"></i>
      </div>

      <div id = "menu-scroll" class="menu visible-lg">
        <ul>
          <li><a href="#" class="start-btn menu-link"><fmt:message key="start"/></a></li>
          <li><a href="#pizza-carousel" class="menu-link"><fmt:message key="pizzahead"/></a></li>
          <li><a href="#salad-carousel" class="menu-link"><fmt:message key="saladhead"/></a></li>
          <li><a href="#drink-carousel" class="menu-link"><fmt:message key="drinkhead"/></a></li>
          <li><button class="registration menu-link"><fmt:message key="login"/></button></li>
          <li><button class="nickblock menu-link"><span class="nick"></span></button></li>
          <li><span class="phone menu-link"><fmt:message key="phone"/><br></span>
            <span class="phone menu-link">+375 25 133-73-22</span>
          </li>
          <li>
            <form action="controller" method="post" class="lang_form menu-link">
              <input type="submit" value="ru" name="ruLanguage" class="lang_btn">
              <span class="menu-link">/</span>
              <input type="submit" value="en" name="enLanguage" class="lang_btn">
              <input type="hidden" name="form" value="locale_form"/>
            </form>
          </li>
        </ul>
      </div>
    </header>

    <div class="main-content">
      <div class="inner-block">
        <div class="pizza-text">
          <h1 class="pizza-name"><fmt:message key="pizzatitle"/><br> <fmt:message key="neapolitana"/></h1>
          <p class="pizza-description"><fmt:message key="neapolitanadesc"/></p>
          <button class="select-button" point = "#pizza-carousel">
          <span class="select-text">
            <fmt:message key="selectbtn"/>
          </span>
          </button>
        </div>

        <div class="pizza-image">
          <img src="resources/images/neapolitana.png" alt="Пицца Неаполитана">
        </div>
      </div>

      <div class="inner-block">
        <div class="pizza-text">
          <h1 class="pizza-name"><fmt:message key="pizzatitle"/><br> <fmt:message key="tropicana"/></h1>
          <p class="pizza-description"><fmt:message key="tropicanadesc"/></p>
          <button class="select-button" point = "#pizza-carousel">
          <span class="select-text">
            <fmt:message key="selectbtn"/>
          </span>
          </button>
        </div>

        <div class="pizza-image">
          <img src="resources/images/tropicana.png" alt="Пицца Тропикана">
        </div>
      </div>
      <div class="inner-block">
        <div class="pizza-text">
          <h1 class="pizza-name"><fmt:message key="pizzatitle"/><br> <fmt:message key="pepperony"/></h1>
          <p class="pizza-description"><fmt:message key="pepperonydesc"/></p>
          <button class="select-button" point = "#pizza-carousel">
          <span class="select-text">
            <fmt:message key="selectbtn"/>
          </span>
          </button>
        </div>

        <div class="pizza-image">
          <img src="resources/images/pepperony.png" alt="Пицца Неаполитана">
        </div>
      </div>

    </div>

    <div class="main-window modal">

      <div class="js-modal-close">
        <i class="fa fa-times"></i>
      </div>
      <div class="flat-design-form">

        <ul class="tabs">
          <li><a class="active_field" href="#login" id="login-tab" name="login-tab"><fmt:message key="userIn"/></a></li>
          <li><a href="#register" id="register-tab" name="register-tab"><fmt:message key="userReg"/></a></li>
        </ul>
      </div>

      <div class="form-display show" id="login">

        <form   id="login-form-id" accept-charset="utf-8">
          <fieldset>
            <span class="result"></span>
            <ul>
              <li>
                <div class="item">
                  <input id="log_username" name="name" placeholder="<fmt:message key="logInName"/>" type="text">
                  <input type="hidden" class="log-form" name="form" value="login"/>
                </div>
              </li>
              <li>
                <div class="item">
                  <input id="log_password" name="password" placeholder="<fmt:message key="logInPassword"/>" type="password">
                </div>
              </li>
              <li><input class="button-login" type="button" value="<fmt:message key="logInSubmit"/>"></li>

            </ul>
          </fieldset>

        </form>
      </div>

      <div class="form-display hide" id="register" >
        <form id = "register-form-id" novalidate="" >
          <fieldset>
            <span class="result-reg"></span>
            <ul>
              <li>
                <div class="item">
                  <input id="reg_username" name="name" placeholder="<fmt:message key="regUsername"/>" type="text" >
                  <input class="reg-form" type="hidden" name="form" value="register"/>
                </div>
              </li>
              <li>
                <div class="item">
                  <input id= "password" name="password"  placeholder="<fmt:message key="regPassword"/>"  type="password">
                </div>
              </li>
              <li>
                <div class="item">
                  <input name="confirm_password"  placeholder="<fmt:message key="regRepeat"/>" type="password">
                </div>
              </li>
              <li>
                <div class="item">
                  <input class='email' id="reg_email" name="email" placeholder="Email"  type="email">
                </div>
              </li>
              <li>
                <div class="item">
                  <input type="tel" id="phone" name="phone" placeholder="<fmt:message key="regPhone"/>" maxlength="13">
                </div>
              </li>
              <li><input class="button-register" id='send' type="button" value="<fmt:message key="regSubmit"/>"></li>
            </ul>
          </fieldset>
        </form>
      </div>
    </div>

    <div class="modal-overlay"></div>

    <div class="arrow-scroll">
      <a href="#pizza-carousel" class="main-arrow">
        <div class="arrow-down"><img src="resources/images/arrow.png" alt="Вниз"></div>
      </a>
    </div>
  </div>

  <div class="second">
    <div class="pizza-selector">
    <span class="pizza-title">
      <b id="pizza-carousel"><fmt:message key="pizcarouseltitle"/></b>
    </span>

      <div class="pizza-carousel" >
        <div class="pizza-inner">
          <div class="pizza-select">
            <img src="resources/images/neapolitana.png" alt="Пицца Неаполитана">
            <div class="pizza-size-26 neapolitana">
              <p class="size-text">26</p>
            </div>
            <div class="pizza-size-32 neapolitana">
              <p class="size-text">32</p>
            </div>
            <div class="price-rectangle">
            <span class="neapolitana">
              14.30р.
            </span>
            </div>

          </div>

          <h3><fmt:message key="neapolitcarousel"/></h3>
          <hr>
          <span class="pizza-ingredients">
          <p><b><fmt:message key="components"/></b> <i><fmt:message key="neapcompo"/></i></p>
        </span>
          <div class="goodorderdiv">
            <button class="updateorder select-pizza-btn neapolitana" data-product-identifier = "1"
                    data-product-price = "14.30" data-product-type="pizza">
              <i class="fa fa-check"></i>
              <fmt:message key="selectbtn"/>
            </button>

            <div class="goodorder">
              <i class="fa fa-check"></i>
            </div>
          </div>
        </div>

        <div class="pizza-inner">
          <div class="pizza-select">
            <img src="resources/images/tropicana.png" alt="Пицца Тропикана">
            <div class="pizza-size-26 tropicana">
              <p class="size-text">26</p>
            </div>
            <div class="pizza-size-32 tropicana">
              <p class="size-text">32</p>
            </div>
            <div class="price-rectangle">
            <span class="tropicana">
              16.50р.
            </span>
            </div>
          </div>
          <h3><fmt:message key="tropiccarousel"/></h3>
          <hr>
          <span class="pizza-ingredients">
          <p><b><fmt:message key="components"/></b> <i><fmt:message key="tropiccompo"/></i></p>
        </span>
          <div class="goodorderdiv">
            <button class="updateorder select-pizza-btn tropicana" data-product-identifier = "3"
                    data-product-price = "16.50" data-product-type="pizza">
              <i class="fa fa-check"></i>
              <fmt:message key="selectbtn"/>
            </button>
            <div class="goodorder">
              <i class="fa fa-check"></i>
            </div>
          </div>

        </div>

        <div class="pizza-inner">
          <div class="pizza-select">
            <img src="resources/images/pepperony.png" alt="Пицца Пепперони">
            <div class="pizza-size-26 pepperony">
              <p class="size-text">26</p>
            </div>
            <div class="pizza-size-32 pepperony">
              <p class="size-text">32</p>
            </div>
            <div class="price-rectangle">
            <span class="pepperony">
              11.70р.
            </span>
            </div>
          </div>
          <h3><fmt:message key="peppercarous"/></h3>
          <hr>
          <span class="pizza-ingredients">
          <p><b><fmt:message key="components"/></b> <i><fmt:message key="peppercompo"/></i></p>
        </span>
          <div class="goodorderdiv">
            <button class="updateorder select-pizza-btn pepperony" data-product-identifier = "5"
                    data-product-price = "11.70" data-product-type="pizza">
              <i class="fa fa-check"></i>
              <fmt:message key="selectbtn"/>
            </button>
            <div class="goodorder">
              <i class="fa fa-check"></i>
            </div>
          </div>
        </div>

        <div class="pizza-inner">
          <div class="pizza-select">
            <img src="resources/images/carbonara.png" alt="Пицца Карбонара">
            <div class="pizza-size-26 carbonara">
              <p class="size-text">26</p>
            </div>
            <div class="pizza-size-32 carbonara">
              <p class="size-text">32</p>
            </div>
            <div class="price-rectangle">
            <span class="carbonara">
              13.30р.
            </span>
            </div>
          </div>
          <h3><fmt:message key="carboncarous"/></h3>
          <hr>
          <span class="pizza-ingredients">
          <p><b><fmt:message key="components"/></b> <i><fmt:message key="carboncompo"/></i></p>
        </span>
          <div class="goodorderdiv">
            <button class="updateorder select-pizza-btn carbonara" data-product-identifier = "7"
                    data-product-price = "13.30" data-product-type="pizza">
              <i class="fa fa-check"></i>
              <fmt:message key="selectbtn"/>
            </button>
            <div class="goodorder">
              <i class="fa fa-check"></i>
            </div>
          </div>
        </div>

        <div class="pizza-inner">
          <div class="pizza-select">
            <img src="resources/images/5_syrov.png" alt="Пицца 5 Сыров">
            <div class="pizza-size-26 5_syrov">
              <p class="size-text">26</p>
            </div>
            <div class="pizza-size-32 5_syrov">
              <p class="size-text">32</p>
            </div>
            <div class="price-rectangle">
            <span class="5_syrov">
              9.30р.
            </span>
            </div>
          </div>
          <h3><fmt:message key="cheesecarous"/></h3>
          <hr>
          <span class="pizza-ingredients">
          <p><b><fmt:message key="components"/></b> <i><fmt:message key="cheescompo"/></i></p>
        </span>
          <div class="goodorderdiv">
            <button class="updateorder select-pizza-btn 5_syrov" data-product-identifier = "9"
                    data-product-price = "9.30" data-product-type="pizza">
              <i class="fa fa-check"></i>
              <fmt:message key="selectbtn"/>
            </button>
            <div class="goodorder">
              <i class="fa fa-check"></i>
            </div>
          </div>
        </div>

        <div class="pizza-inner">
          <div class="pizza-select">
            <img src="resources/images/margarita.png" alt="Пицца Маргарита">
            <div class="pizza-size-26 margarita">
              <p class="size-text">26</p>
            </div>
            <div class="pizza-size-32 margarita">
              <p class="size-text">32</p>
            </div>
            <div class="price-rectangle">
            <span class="margarita">
              12.30р.
            </span>
            </div>
          </div>
          <h3><fmt:message key="margarcarous"/></h3>
          <hr>
          <span class="pizza-ingredients">
          <p><b><fmt:message key="components"/></b> <i><fmt:message key="margarcompo"/></i></p>
        </span>
          <div class="goodorderdiv">
            <button class="updateorder select-pizza-btn margarita" data-product-identifier = "11"
                    data-product-price = "12.30" data-product-type="pizza">
              <i class="fa fa-check"></i>
              <fmt:message key="selectbtn"/>
            </button>
            <div class="goodorder">
              <i class="fa fa-check"></i>
            </div>
          </div>
        </div>

        <div class="pizza-inner">
          <div class="pizza-select">
            <img src="resources/images/belorusskaya.png" alt="Пицца Белорусская">
            <div class="pizza-size-26 belorusskaya">
              <p class="size-text">26</p>
            </div>
            <div class="pizza-size-32 belorusskaya">
              <p class="size-text">32</p>
            </div>
            <div class="price-rectangle">
            <span class="belorusskaya">
              19.30р.
            </span>
            </div>
          </div>
          <h3><fmt:message key="belcarous"/></h3>
          <hr>
          <span class="pizza-ingredients">
          <p><b><fmt:message key="components"/></b><i><fmt:message key="belcompo"/></i></p>
        </span>
          <div class="goodorderdiv">
            <button class="updateorder select-pizza-btn belorusskaya" data-product-identifier = "13"
                    data-product-price = "19.30" data-product-type="pizza">
              <i class="fa fa-check"></i>
              <fmt:message key="selectbtn"/>
            </button>
            <div class="goodorder">
              <i class="fa fa-check"></i>
            </div>
          </div>
        </div>

        <div class="pizza-inner">
          <div class="pizza-select">
            <img src="resources/images/kantri.png" alt="Пицца Кантри">
            <div class="pizza-size-26 kantri">
              <p class="size-text">26</p>
            </div>
            <div class="pizza-size-32 kantri">
              <p class="size-text">32</p>
            </div>
            <div class="price-rectangle">
            <span class="kantri">
              17.30р.
            </span>
            </div>
          </div>
          <h3><fmt:message key="cantricar"/></h3>
          <hr>
          <span class="pizza-ingredients">
          <p><b><fmt:message key="components"/></b> <i><fmt:message key="countrcompo"/></i></p>
        </span>
          <div class="goodorderdiv">
            <button class="updateorder select-pizza-btn kantri" data-product-identifier = "15"
                    data-product-price = "17.30" data-product-type="pizza">
              <i class="fa fa-check"></i>
              <fmt:message key="selectbtn"/>
            </button>
            <div class="goodorder">
              <i class="fa fa-check"></i>
            </div>
          </div>
        </div>

        <div class="pizza-inner">
          <div class="pizza-select">
            <img src="resources/images/toskana.png" alt="Пицца Тоскана">
            <div class="pizza-size-26 toskana">
              <p class="size-text">26</p>
            </div>
            <div class="pizza-size-32 toskana">
              <p class="size-text">32</p>
            </div>
            <div class="price-rectangle">
            <span class="toskana">
              11.60р.
            </span>
            </div>
          </div>
          <h3><fmt:message key="toskcarous"/></h3>
          <hr>
          <span class="pizza-ingredients">
          <p><b><fmt:message key="components"/></b> <i><fmt:message key="toskcompo"/></i></p>
        </span>
          <div class="goodorderdiv">
            <button class="updateorder select-pizza-btn toskana" data-product-identifier = "17"
                    data-product-price = "11.60" data-product-type="pizza">
              <i class="fa fa-check"></i>
              <fmt:message key="selectbtn"/>
            </button>
            <div class="goodorder">
              <i class="fa fa-check"></i>
            </div>
          </div>
        </div>
      </div>

      <div class="arrow-scroll">
        <a href="#salad-carousel" class="additionalarrow main-arrow">
          <div class="arrow-down"><img src="resources/images/arrow.png" alt="Вниз"></div>
        </a>
      </div>
    </div>

  </div>

  <div class="third">
    <div class="saladoverlay main-overlay"></div>
    <div class="saladbg bg-image"></div>
    <div class="salad-content">
    <span class="salad-title">
      <b id="salad-carousel"><fmt:message key="saladcarousetitle"/></b>
    </span>
      <span class="salad-desc">
      <fmt:message key="saladcardesc"/>
    </span>
      <div class="salad-carousel">

        <div class="salad-inner">
          <div class="salad-info">
            <img src="resources/images/salat1.jpg" alt="Салат Греческий">
            <div class="salad-price">
            <span class="price-text">
              <p>6.9р.</p>
              <p>190<fmt:message key="saladgr"/></p>
            </span>
            </div>
          </div>
          <div class="salad-text">
            <p><fmt:message key="gretitle"/></p>
            <p class="salad-text-comment"><fmt:message key="gredesc"/></p>
          </div>

          <span class="salad-ingredients">
          <p><b><fmt:message key="components"/></b> <i><fmt:message key="grecompo"/></i></p>
        </span>
          <div class="goodorderdiv">
            <button class="updateorder salad-select" data-product-identifier = "1"
                    data-product-price = "6.90" data-product-type="salad">
              <i class="fa fa-check"></i>
              <fmt:message key="selectbtn"/>
            </button>
            <div class="goodorder">
              <i class="fa fa-check"></i>
            </div>
          </div>

        </div>

        <div class="salad-inner">
          <div class="salad-info">
            <img src="resources/images/salat2.jpg" alt="Салат Монте-Кристо">
            <div class="salad-price">
            <span class="price-text">
              <p>7.4р.</p>
              <p>120<fmt:message key="saladgr"/></p>
            </span>
            </div>
          </div>
          <div class="salad-text">
            <p><fmt:message key="montetitle"/></p>
            <p class="salad-text-comment"><fmt:message key="montedesc"/></p>
          </div>
          <span class="salad-ingredients">
          <p><b><fmt:message key="components"/></b> <i><fmt:message key="montecompo"/></i></p>
        </span>
          <div class="goodorderdiv">
            <button class="updateorder salad-select" data-product-identifier = "2"
                    data-product-price = "7.40" data-product-type="salad">
              <i class="fa fa-check"></i>
              <fmt:message key="selectbtn"/>
            </button>
            <div class="goodorder">
              <i class="fa fa-check"></i>
            </div>
          </div>
        </div>

        <div class="salad-inner">
          <div class="salad-info">
            <img src="resources/images/salat3.jpg" alt="Салат Оливье">
            <div class="salad-price">
            <span class="price-text">
              <p>5р.</p>
              <p>150<fmt:message key="saladgr"/></p>
            </span>
            </div>
          </div>
          <div class="salad-text">
            <p><fmt:message key="olivtitle"/></p>
            <p class="salad-text-comment"><fmt:message key="olidesc"/></p>
          </div>
          <span class="salad-ingredients">
          <p><b><fmt:message key="components"/></b> <i><fmt:message key="olicompo"/></i></p>
        </span>
          <div class="goodorderdiv">
            <button class="updateorder salad-select" data-product-identifier = "3"
                    data-product-price = "5" data-product-type="salad">
              <i class="fa fa-check"></i>
              <fmt:message key="selectbtn"/>
            </button>
            <div class="goodorder">
              <i class="fa fa-check"></i>
            </div>
          </div>
        </div>

        <div class="salad-inner">
          <div class="salad-info">
            <img src="resources/images/salat4.jpg" alt="Салат Цезарь">
            <div class="salad-price">
            <span class="price-text">
              <p>7.4р.</p>
              <p>200<fmt:message key="saladgr"/></p>
            </span>
            </div>
          </div>
          <div class="salad-text">
            <p><fmt:message key="ceztitle"/></p>
            <p class="salad-text-comment"><fmt:message key="cezdesc"/></p>
          </div>
          <span class="salad-ingredients">
          <p><b><fmt:message key="components"/></b> <i><fmt:message key="cezcompo"/></i></p>
        </span>
          <div class="goodorderdiv">
            <button class="updateorder salad-select" data-product-identifier = "4"
                    data-product-price = "7.40" data-product-type="salad">
              <i class="fa fa-check"></i>
              <fmt:message key="selectbtn"/>
            </button>
            <div class="goodorder">
              <i class="fa fa-check"></i>
            </div>
          </div>
        </div>
      </div>

      <div class="arrow-scroll">
        <a href="#drink-carousel" class="additionalarrow main-arrow">
          <div class="arrow-down"><img src="resources/images/arrow.png" alt="Вниз"></div>
        </a>
      </div>
    </div>
  </div>

  <div class="fourth">
    <div class="drink-content">
    <span class="drink-title">
      <b id="drink-carousel"><fmt:message key="drincartitle"/></b>
    </span>
      <span class="drink-desc">
      <fmt:message key="drinkdesc"/>
    </span>
      <div class="drink-carousel">
        <div class="drink-inner">
          <img src="resources/images/sprite.png" alt="Спрайт">
          <p class="drink-name"><fmt:message key="sprite"/>, 1.5 <fmt:message key="drinkV"/></p>
          <p class="drink-price">3.90 р.</p>
          <div class="goodorderdiv">
            <button class="updateorder select-drink-btn" data-product-identifier = "2"
                    data-product-price = "3.90" data-product-type="drink">
              <i class="fa fa-check"></i>
              <fmt:message key="selectbtn"/>
            </button>
            <div class="goodorder">
              <i class="fa fa-check"></i>
            </div>
          </div>
        </div>

        <div class="drink-inner">
          <img src="resources/images/aqua.png" alt="Aqua Minerale">
          <p class="drink-name">Aqua Minerale, 0.5 <fmt:message key="drinkV"/></p>
          <p class="drink-price">1.50 р.</p>
          <div class="goodorderdiv">
            <button class="updateorder select-drink-btn" data-product-identifier = "3"
                    data-product-price = "1.50" data-product-type="drink">
              <i class="fa fa-check"></i>
              <fmt:message key="selectbtn"/>
            </button>
            <div class="goodorder">
              <i class="fa fa-check"></i>
            </div>
          </div>
        </div>

        <div class="drink-inner">
          <img src="resources/images/kvas.png" alt="Квас">
          <p class="drink-name"><fmt:message key="kvass"/> 0,9 <fmt:message key="drinkV"/></p>
          <p class="drink-price">2 р.</p>
          <div class="goodorderdiv">
            <button  class="updateorder select-drink-btn" data-product-identifier = "4"
                     data-product-price = "2" data-product-type="drink">
              <i class="fa fa-check"></i>
              <fmt:message key="selectbtn"/>
            </button>
            <div class="goodorder">
              <i class="fa fa-check"></i>
            </div>
          </div>
        </div>

        <div class="drink-inner">
          <img src="resources/images/juice.png" alt="Сок">
          <p class="drink-name"><fmt:message key="juice"/>, 0.97 <fmt:message key="drinkV"/></p>
          <p class="drink-price">5.60 р.</p>
          <div class="goodorderdiv">
            <button class="updateorder select-drink-btn" data-product-identifier = "5"
                    data-product-price = "5.60" data-product-type="drink">
              <i class="fa fa-check"></i>
              <fmt:message key="selectbtn"/>
            </button>
            <div class="goodorder">
              <i class="fa fa-check"></i>
            </div>
          </div>
        </div>

        <div class="drink-inner">
          <img src="resources/images/tea.png" alt="Липтон">
          <p class="drink-name"><fmt:message key="lipton"/>, 0.5 <fmt:message key="drinkV"/></p>
          <p class="drink-price">2.20 р.</p>
          <div class="goodorderdiv">
            <button class="updateorder select-drink-btn" data-product-identifier = "6"
                    data-product-price = "2.20" data-product-type="drink">
              <i class="fa fa-check"></i>
              <fmt:message key="selectbtn"/>
            </button>
            <div class="goodorder">
              <i class="fa fa-check"></i>
            </div>
          </div>
        </div>

        <div class="drink-inner">
          <img src="resources/images/pepsi.png" alt="Пепси">
          <p class="drink-name">Pepsi, 0.5 <fmt:message key="drinkV"/></p>
          <p class="drink-price">2.20 р.</p>
          <div class="goodorderdiv">
            <button class="updateorder select-drink-btn" data-product-identifier = "1"
                    data-product-price = "2.20" data-product-type="drink">
              <i class="fa fa-check"></i>
              <fmt:message key="selectbtn"/>
            </button>
            <div class="goodorder">
              <i class="fa fa-check"></i>
            </div>
          </div>
        </div>
      </div>

      <div class="arrow-scroll">
        <a href="#order-container" class="additionalarrow main-arrow">
          <div class="arrow-down"><img src="resources/images/arrow.png" alt="Вниз"></div>
        </a>
      </div>
    </div>
  </div>

  <footer>
    <div class="uptext">
      <span class="footertitle"><fmt:message key="upordertitle"/></span>
      <span class="footerdesc"><fmt:message key="updesc"/><br><br><fmt:message key="upaction"/></span>
    </div>

    <div id = "order-container" class="order-container">
      <div class="user-price-invis"></div>
      <div class="order-content">
        <div class="order">
          <div class="stroke">
          <span class="white-text"><b><fmt:message key="ordertitle1"/></b> <span class="orange-text">
            <b><fmt:message key="ordertitle2"/></b></span></span>
            <div class="order-items">
              <ul class="ul-order">

                <c:forEach var="product" items = "${sessionScope.ajaxOrder}">
                  <c:set var="dataValue" value=""/>
                  <c:choose>
                    <c:when test="${sessionScope.locale eq 'ru'}">
                      <c:set var="productName" value="${product.productName}"/>
                    </c:when>
                    <c:when test="${sessionScope.locale eq 'en'}">
                      <c:set var="productName" value="${product.enName}"/>
                    </c:when>
                  </c:choose>
                  <c:choose>
                    <c:when test="${product.productType eq 'PIZZA'}">
                      <c:set var="dataValue" value="pizza:${product.productID}"/>
                    </c:when>
                    <c:when test="${product.productType eq 'DRINK'}">
                      <c:set var="dataValue" value="drink:${product.productID}"/>
                    </c:when>
                    <c:when test="${product.productType eq 'SALAD'}">
                      <c:set var="dataValue" value="salad:${product.productID}"/>
                    </c:when>
                  </c:choose>
                  <li class = 'order-li' data-value = '${dataValue}'>
                  <span class = 'clearitem' onclick = onLiClick(this);
                  >${productName} ${product.size} ${product.price}</span>
                  </li>
                </c:forEach>
              </ul>
            </div>
            <hr>
            <span class="price white-text">
            <fmt:message key="totalprice"/>
            <span class="price orange-text">
              <c:choose>
                <c:when test="${sessionScope.orderPrice != null}">
                  <div class="number-price">${sessionScope.orderPrice}</div>
                  р.
                </c:when>
                <c:otherwise>
                  <div class="number-price">0.00</div>
                  р.
                </c:otherwise>
              </c:choose>

            </span>
          </span>
            <p class="order-good"></p>

            <div class="order-buttons">
              <button class="drop-btn">
                <i class="fa fa-times"></i>
                <fmt:message key="resetorder"/>
              </button>
              <button class="formalize-btn">
                <i class="fa fa-check"></i>
                <fmt:message key="submitorder"/>
              </button>
            </div>
          </div>
        </div>
        <div class="comments">
          <img src="resources/images/pizza_bon.png" alt="Bon Appetit">
          <img src="resources/images/text_bon.png" alt="Bon Appetit">
        </div>
      </div>

    </div>
    <div class="downtext">
      <div class="info">
      <span class="first-title">
        <p class="title"><fmt:message key="foottitle"/></p>
        <p class="simpledesc"><fmt:message key="footdesc"/></p>

        <a href="pizza@mania.com">
          <i class="fa fa-envelope"></i>
          pizza@mania.com
        </a>

        <p class=".footerphone"><i class="fa fa-phone-square"></i>+375 25 133-73-22</p>
        <p class="address">
          <i class="fa fa-map-marker"></i>
          <fmt:message key="footaddress"/>
        </p>

      </span>
        <span class="second-title">
        <p class="title"><fmt:message key="footdeliv"/></p>
        <div class="footer-menu menu">
          <ul>
            <li><a href="#menu-scroll" class="start-btn menu-link"><fmt:message key="start"/></a></li>
            <li><a href="#pizza-carousel" class="menu-link"><fmt:message key="pizzahead"/></a></li>
            <li><a href="#salad-carousel" class="menu-link"><fmt:message key="saladhead"/></a></li>
            <li><a href="#drink-carousel" class="menu-link"><fmt:message key="drinkhead"/></a></li>
            <li><button class="registration menu-link"><fmt:message key="login"/></button></li>
          </ul>
        </div>
      </span>
        <span class="third-title">
        <p class="title"><fmt:message key="footlink"/></p>
        <ul>
          <li>
            <a href="https://vk.com/" target="_blank">
              <img src="resources/images/vklogo.png" alt="ВКонтакте">
            </a>
          </li>
          <li>
            <a href="https://www.facebook.com/" target="_blank">
              <img src="resources/images/facebooklogo.png" alt="Фейсбук">
            </a>
          </li>
          <li>
            <a href="https://twitter.com/" target="_blank">
              <img src="resources/images/twitterlogo.png" alt="Твиттер">
            </a>
          </li>
          <li>
            <a href="https://www.linkedin.com/" target="_blank">
              <img src="resources/images/linkedlogo.png" alt="LinkedIn">
            </a>
          </li>
        </ul>
      </span>
      </div>
    </div>
  </footer>

  <form id="forwardform" action="controller" method="post" hidden>
    <input id = "forwarddirection" type="text" value="" name="direction"/>
    <input type="hidden" name="form" value="sendforward"/>
  </form>

  <c:if test="${sessionScope.login == true}">
    <script>
      $(function () {
        var registerelems=document.getElementsByClassName('registration');
        for(var i=0; i<registerelems.length; i++)registerelems[i].style.display='none';

        var userelems=document.getElementsByClassName('nickblock');
        for(var i=0; i<userelems.length; i++)userelems[i].style.display='inline-block';


        $(".nick").text('${sessionScope.client.name}');

        $(".user-price-invis").text('${sessionScope.client.discount}');
      });
    </script>
  </c:if>
  </body>
  </html>
</fmt:bundle>