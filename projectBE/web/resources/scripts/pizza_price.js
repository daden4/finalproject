$(function(){
	var size_26_neapolitana = $(".pizza-size-26.neapolitana");
	var size_32_neapolitana = $(".pizza-size-32.neapolitana");
	var price_neapolitana = $(".price-rectangle .neapolitana");
	var neapolitanaId = $('.updateorder.select-pizza-btn.neapolitana');

	size_26_neapolitana.toggleClass('size-back');
	size_26_neapolitana.on('click', function(event) {
		event.preventDefault();
		if(size_32_neapolitana.hasClass('size-back')){
			size_32_neapolitana.toggleClass('size-back');
		}
		if(!size_26_neapolitana.hasClass('size-back')){
			size_26_neapolitana.toggleClass('size-back');
			price_neapolitana.text('14.30р.');
		}

		neapolitanaId.attr("data-product-identifier", 1);
		neapolitanaId.attr("data-product-price", 14.30);

	});

	size_32_neapolitana.on('click', function(event) {
		event.preventDefault();
		if(size_26_neapolitana.hasClass('size-back')){
			size_26_neapolitana.toggleClass('size-back');
		}
		if(!size_32_neapolitana.hasClass('size-back')){
			size_32_neapolitana.toggleClass('size-back');
			price_neapolitana.text('22.50р.');
		}

		neapolitanaId.attr("data-product-identifier", 2);
		neapolitanaId.attr("data-product-price", 22.50);
	});




	var size_26_tropicana = $(".pizza-size-26.tropicana");
	var size_32_tropicana = $(".pizza-size-32.tropicana");
	var price_tropicana = $(".price-rectangle .tropicana");
	var tropicanaId = $('.updateorder.select-pizza-btn.tropicana');

	size_26_tropicana.toggleClass('size-back');
	size_26_tropicana.on('click', function(event) {
		event.preventDefault();
		if(size_32_tropicana.hasClass('size-back')){
			size_32_tropicana.toggleClass('size-back');
		}
		if(!size_26_tropicana.hasClass('size-back')){
			size_26_tropicana.toggleClass('size-back');
			price_tropicana.text('16.50р.');
		}
		tropicanaId.attr("data-product-identifier", 3);
		tropicanaId.attr("data-product-price", 16.50);
	});

	size_32_tropicana.on('click', function(event) {
		event.preventDefault();
		if(size_26_tropicana.hasClass('size-back')){
			size_26_tropicana.toggleClass('size-back');
		}
		if(!size_32_tropicana.hasClass('size-back')){
			size_32_tropicana.toggleClass('size-back');
			price_tropicana.text('24.20р.');
		}
		tropicanaId.attr("data-product-identifier", 4);
		tropicanaId.attr("data-product-price", 24.20);
	});



	var size_26_pepperony = $(".pizza-size-26.pepperony");
	var size_32_pepperony = $(".pizza-size-32.pepperony");
	var price_pepperony = $(".price-rectangle .pepperony");
	var pepperonyId = $('.updateorder.select-pizza-btn.pepperony');

	size_26_pepperony.toggleClass('size-back');
	size_26_pepperony.on('click', function(event) {
		event.preventDefault();
		if(size_32_pepperony.hasClass('size-back')){
			size_32_pepperony.toggleClass('size-back');
		}
		if(!size_26_pepperony.hasClass('size-back')){
			size_26_pepperony.toggleClass('size-back');
			price_pepperony.text('11.70р.');
		}

		pepperonyId.attr("data-product-identifier", 5);
		pepperonyId.attr("data-product-price", 11.70);
	});

	size_32_pepperony.on('click', function(event) {
		event.preventDefault();
		if(size_26_pepperony.hasClass('size-back')){
			size_26_pepperony.toggleClass('size-back');
		}
		if(!size_32_pepperony.hasClass('size-back')){
			size_32_pepperony.toggleClass('size-back');
			price_pepperony.text('19.70р.');
		}

		pepperonyId.attr("data-product-identifier", 6);
		pepperonyId.attr("data-product-price", 19.70);
	});



	var size_26_carbonara = $(".pizza-size-26.carbonara");
	var size_32_carbonara = $(".pizza-size-32.carbonara");
	var price_carbonara = $(".price-rectangle .carbonara");
	var carbonaraId = $('.updateorder.select-pizza-btn.carbonara');

	size_26_carbonara.toggleClass('size-back');
	size_26_carbonara.on('click', function(event) {
		event.preventDefault();
		if(size_32_carbonara.hasClass('size-back')){
			size_32_carbonara.toggleClass('size-back');
		}
		if(!size_26_carbonara.hasClass('size-back')){
			size_26_carbonara.toggleClass('size-back');
			price_carbonara.text('13.30р.');
		}

		carbonaraId.attr("data-product-identifier", 7);
		carbonaraId.attr("data-product-price", 13.30);

	});

	size_32_carbonara.on('click', function(event) {
		event.preventDefault();
		if(size_26_carbonara.hasClass('size-back')){
			size_26_carbonara.toggleClass('size-back');
		}
		if(!size_32_carbonara.hasClass('size-back')){
			size_32_carbonara.toggleClass('size-back');
			price_carbonara.text('29.50р.');
		}

		carbonaraId.attr("data-product-identifier", 8);
		carbonaraId.attr("data-product-price", 29.50);
	});





	var size_26_5_syrov = $(".pizza-size-26.5_syrov");
	var size_32_5_syrov = $(".pizza-size-32.5_syrov");
	var price_5_syrov = $(".price-rectangle .5_syrov");
	var syrsId = $('.updateorder.select-pizza-btn.5_syrov');

	size_26_5_syrov.toggleClass('size-back');
	size_26_5_syrov.on('click', function(event) {
		event.preventDefault();
		if(size_32_5_syrov.hasClass('size-back')){
			size_32_5_syrov.toggleClass('size-back');
		}
		if(!size_26_5_syrov.hasClass('size-back')){
			size_26_5_syrov.toggleClass('size-back');
			price_5_syrov.text('9.30р.');
		}

		syrsId.attr("data-product-identifier", 9);
		syrsId.attr("data-product-price", 9.30);

	});

	size_32_5_syrov.on('click', function(event) {
		event.preventDefault();
		if(size_26_5_syrov.hasClass('size-back')){
			size_26_5_syrov.toggleClass('size-back');
		}
		if(!size_32_5_syrov.hasClass('size-back')){
			size_32_5_syrov.toggleClass('size-back');
			price_5_syrov.text('15.50р.');
		}

		syrsId.attr("data-product-identifier", 10);
		syrsId.attr("data-product-price", 15.50);
	});



	var size_26_margarita = $(".pizza-size-26.margarita");
	var size_32_margarita = $(".pizza-size-32.margarita");
	var price_margarita = $(".price-rectangle .margarita");
	var margaritaId = $('.updateorder.select-pizza-btn.margarita');

	size_26_margarita.toggleClass('size-back');
	size_26_margarita.on('click', function(event) {
		event.preventDefault();
		if(size_32_margarita.hasClass('size-back')){
			size_32_margarita.toggleClass('size-back');
		}
		if(!size_26_margarita.hasClass('size-back')){
			size_26_margarita.toggleClass('size-back');
			price_margarita.text('12.30р.');
		}

		margaritaId.attr("data-product-identifier", 11);
		margaritaId.attr("data-product-price", 12.30);

	});

	size_32_margarita.on('click', function(event) {
		event.preventDefault();
		if(size_26_margarita.hasClass('size-back')){
			size_26_margarita.toggleClass('size-back');
		}
		if(!size_32_margarita.hasClass('size-back')){
			size_32_margarita.toggleClass('size-back');
			price_margarita.text('21.80р.');
		}

		margaritaId.attr("data-product-identifier", 12);
		margaritaId.attr("data-product-price", 21.80);
	});


	var size_26_belorusskaya = $(".pizza-size-26.belorusskaya");
	var size_32_belorusskaya = $(".pizza-size-32.belorusskaya");
	var price_belorusskaya = $(".price-rectangle .belorusskaya");
	var belorusskayaId = $('.updateorder.select-pizza-btn.belorusskaya');

	size_26_belorusskaya.toggleClass('size-back');
	size_26_belorusskaya.on('click', function(event) {
		event.preventDefault();
		if(size_32_belorusskaya.hasClass('size-back')){
			size_32_belorusskaya.toggleClass('size-back');
		}
		if(!size_26_belorusskaya.hasClass('size-back')){
			size_26_belorusskaya.toggleClass('size-back');
			price_belorusskaya.text('19.30р.');
		}

		belorusskayaId.attr("data-product-identifier", 13);
		belorusskayaId.attr("data-product-price", 19.30);

	});

	size_32_belorusskaya.on('click', function(event) {
		event.preventDefault();
		if(size_26_belorusskaya.hasClass('size-back')){
			size_26_belorusskaya.toggleClass('size-back');
		}
		if(!size_32_belorusskaya.hasClass('size-back')){
			size_32_belorusskaya.toggleClass('size-back');
			price_belorusskaya.text('29.90р.');
		}

		belorusskayaId.attr("data-product-identifier", 14);
		belorusskayaId.attr("data-product-price", 29.90);
	});

	var size_26_kantri = $(".pizza-size-26.kantri");
	var size_32_kantri = $(".pizza-size-32.kantri");
	var price_kantri = $(".price-rectangle .kantri");
	var kantriId = $('.updateorder.select-pizza-btn.kantri');

	size_26_kantri.toggleClass('size-back');
	size_26_kantri.on('click', function(event) {
		event.preventDefault();
		if(size_32_kantri.hasClass('size-back')){
			size_32_kantri.toggleClass('size-back');
		}
		if(!size_26_kantri.hasClass('size-back')){
			size_26_kantri.toggleClass('size-back');
			price_kantri.text('17.30р.');
		}

		kantriId.attr("data-product-identifier", 15);
		kantriId.attr("data-product-price", 17.30);

	});

	size_32_kantri.on('click', function(event) {
		event.preventDefault();
		if(size_26_kantri.hasClass('size-back')){
			size_26_kantri.toggleClass('size-back');
		}
		if(!size_32_kantri.hasClass('size-back')){
			size_32_kantri.toggleClass('size-back');
			price_kantri.text('25.50р.');
		}

		kantriId.attr("data-product-identifier", 16);
		kantriId.attr("data-product-price", 25.50);
	});


	var size_26_toskana = $(".pizza-size-26.toskana");
	var size_32_toskana = $(".pizza-size-32.toskana");
	var price_toskana = $(".price-rectangle .toskana");
	var toskanaId = $('.updateorder.select-pizza-btn.toskana');

	size_26_toskana.toggleClass('size-back');
	size_26_toskana.on('click', function(event) {
		event.preventDefault();
		if(size_32_toskana.hasClass('size-back')){
			size_32_toskana.toggleClass('size-back');
		}
		if(!size_26_toskana.hasClass('size-back')){
			size_26_toskana.toggleClass('size-back');
			price_toskana.text('11.60р.');
		}

		toskanaId.attr("data-product-identifier", 17);
		toskanaId.attr("data-product-price", 11.60);

	});

	size_32_toskana.on('click', function(event) {
		event.preventDefault();
		if(size_26_toskana.hasClass('size-back')){
			size_26_toskana.toggleClass('size-back');
		}
		if(!size_32_toskana.hasClass('size-back')){
			size_32_toskana.toggleClass('size-back');
			price_toskana.text('16.20р.');
		}

		toskanaId.attr("data-product-identifier", 18);
		toskanaId.attr("data-product-price", 16.20);
	});

});