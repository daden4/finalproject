$(function(){
	var link = $('.show-menu-btn');
	var menu = $('.m-menu');
	var close = $('.hide-menu-btn');

	var modal = $('.modal');
	var modal_open = $('.registration');
	var modal_close = $('.js-modal-close');
	var modal_over = $('.modal-overlay');
	var main_window = $('.main-window');


	link.on('click', function(event) {
		event.preventDefault();
		menu.toggleClass('m-menu__active');
	});

	close.on('click', function(event) {
		event.preventDefault();
		menu.toggleClass('m-menu__active');
	});

	modal_close.on('click', function(event) {
		event.preventDefault();
		modal.toggleClass('active');
		modal_over.toggleClass('active');
	});

	modal_open.on('click', function(event) {
		event.preventDefault();
		modal.toggleClass('active');
		modal_over.toggleClass('active');
	});

	window.onclick = function(event){
		if ($(event.target).attr('class') == 'modal-overlay active') {
			main_window.toggleClass('active');
			modal_over.toggleClass('active');
		}
	};

	var SHOW_CLASS = 'show',
		HIDE_CLASS = 'hide',
		ACTIVE_CLASS = 'active_field';
	var login_css = document.getElementById("login-tab");
	var reg_css = document.getElementById("register-tab");
	$('.modal').on('click', 'li a', function(e) {
		e.preventDefault();
		var $tab = $(this),
			href = $tab.attr('href');
		$('.active_field').removeClass(ACTIVE_CLASS);
		$tab.addClass(ACTIVE_CLASS);
		$('.show').removeClass(SHOW_CLASS).addClass(HIDE_CLASS).hide();
		$(href).removeClass(HIDE_CLASS).addClass(SHOW_CLASS).hide().fadeIn(620);

		if ($(event.target).attr('id') == 'register-tab') {
			login_css.style.cssText="background:#C59D5F;";
			reg_css.style.cssText="background:#af8043;";
		}
		if ($(event.target).attr('id') == 'login-tab') {
			login_css.style.cssText="background:#91826e;";
			reg_css.style.cssText="background:#C59D5F; 	padding: 1vh 2.5vh 1vh 2.5vh";
		}
	});


	$(".menu").on("click","a", function (event) {
		event.preventDefault();
		var id  = $(this).attr('href'),
			top = $(id).offset().top;
		$('body,html').animate({scrollTop: top}, 1500);
	});

	$(".arrow-scroll").on("click","a", function (event) {
		event.preventDefault();
		var id  = $(this).attr('href'),
			top = $(id).offset().top;
		$('body,html').animate({scrollTop: top}, 500);
	});

	$(".inner-block").on("click","button", function (event) {
		event.preventDefault();
		var id  = $(this).attr('point'),
			top = $(id).offset().top;
		$('body,html').animate({scrollTop: top}, 500);
	});
});

jQuery(function($){
	let user_menu_open = document.querySelector('.nickblock');
	let user_menu = document.querySelector('.user_menu');

	const toggleMenu = () => {
		user_menu.classList.toggle('active');
	}

	user_menu_open.addEventListener('click', e => {
		e.stopPropagation();

		toggleMenu();
	});

	document.addEventListener('click', e => {
		let target = e.target;
		let its_menu = target == user_menu || user_menu.contains(target);
		let its_user_menu_open = target == user_menu_open;
		let menu_is_active = user_menu.classList.contains('active');

		if (!its_menu && !its_user_menu_open && menu_is_active) {
			toggleMenu();
		}
	});
});






