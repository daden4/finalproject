var language;
var validLogReq;
var validLogLen;
var ajaxReq;
var ajaxOrderError;
var ajaxLoginIncor;
var ajaxReqSuccess;
var ajaxOrderSuccess;
var ajaxChangeSuccess;
var validRegReq;
var validRegNameMax;
var validReqNameMin;
var validReqMailMax;
var validRegPassMax;
var validRegPassMin;
var validRegRepEqu;
var validLogPasReq;
var validLogPasMin;
var validChangeMailMax;
var validChangePassMax;
var validChangePassMin;
var validFuncPass;
var validFuncMail;
var validFuncPhone;
var validFuncName;
$(function () {
    if ($('#pageLanguage').text() === "ru"){
        validLogReq = "Введите логин!";
        validLogLen = "Логин содержит минимум 5 символов!"
        ajaxReq = "Заполните все поля!"
        ajaxOrderError = "Ошибка"
        ajaxLoginIncor = "Неверный логин или пароль."
        ajaxReqSuccess = "Успешная регистрация! Спасибо Вам."
        ajaxOrderSuccess = "Заказ успешно обработан, спасибо!"
        ajaxChangeSuccess = "Данные обновлены."
        validRegReq = "Это обязательное поле!"
        validRegNameMax = "Логин должен содержать минимум 5 символов!"
        validReqNameMin = "Слишком длинное имя!"
        validReqMailMax = "Пожалуйста, выберите почту короче."
        validRegPassMax = "Пожалуйста, ограничьтесь 20-ю символами"
        validRegPassMin = "Пароль должен содержать минимум 6 символов!"
        validRegRepEqu = "Пароли не совпадают!"
        validLogPasReq = "Введите пароль!"
        validLogPasMin = "Пароль содержит минимум 6 символов!"
        validChangeMailMax = "Пожалуйста, выберите почту короче."
        validChangePassMax = "Пожалуйста, ограничьтесь 20-ю символами"
        validChangePassMin = "Пароль должен содержать минимум 6 символов!"
        validFuncPass = "Некорректный пароль: не менее 6 символов, не менее одной буквы в каждом регистре, не менее одной цифры"
        validFuncMail = "Некорректный email."
        validFuncPhone = "Введите правильный номер! +375xxXXXXXXX"
        validFuncName = "Некорректный логин: первый символ - латинская буква,минимум 5 символов, разрешено \"_\""
    } else if ($('#pageLanguage').text() === "en") {
        validLogReq = "Enter login!";
        validLogLen = "Login contains at least 5 characters!"
        ajaxReq = "Fill all fields!"
        ajaxOrderError = "Error"
        ajaxLoginIncor = "Incorrect login or password."
        ajaxReqSuccess = "Successful registration! Thank you."
        ajaxOrderSuccess = "The order successfully processed, thank you!"
        ajaxChangeSuccess = "Data has updated."
        validRegReq = "This is a required field!"
        validRegNameMax = "Login contains at least 5 characters!"
        validReqNameMin = "Too long!"
        validReqMailMax = "Please select the mail in short."
        validRegPassMax = "Please limit yourself to 20 characters"
        validRegPassMin = "The password must contain at least 6 characters!"
        validRegRepEqu = "Passwords do not match!"
        validLogPasReq = "Enter the password!"
        validLogPasMin = "The password contains at least 6 characters!"
        validChangeMailMax = "Please select the mail in short."
        validChangePassMax = "Please limit yourself to 20 characters"
        validChangePassMin = "The password must contain at least 6 characters!"
        validFuncPass = "Incorrect password: at least 6 characters, at least one letter in each case, at least one digit"
        validFuncMail = "Incorrect email."
        validFuncPhone = "Enter the correct number! +375xxXXXXXXX"
        validFuncName = "Invalid login: first character - latin letter,minimum 5 characters, allowed \"_\""
    }
});