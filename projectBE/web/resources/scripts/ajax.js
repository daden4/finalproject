
function onLiClick(this_span){
    var product_info = $(this_span).text();
    var product_info_arr = product_info.split(" ");
    var li_index = $($(this_span).parent()).index();
    var product_price = product_info_arr[product_info_arr.length-1];
    var discount = $('.user-price-invis').text();
    var currentPrice = $('.number-price').text();
    var new_price = currentPrice - (product_price-product_price*discount/100);
    var price_fixed = Math.abs(new_price).toFixed(2);
    $.ajax(
        {
            type: 'POST',
            data: {
                orderPrice:price_fixed,
                form: "ul-ajax",
                index:li_index,
            },
            url: "controller",
            success: function (result) {
                $(this_span).parent().remove();
                $('.number-price').text(price_fixed);
            },
            error: function (jqXHR) {
                $('.order-good').text(ajaxOrderError);
            }
        }
    )
}

$(document).ready(function () {
    $('.button-login').click(function () {
        var log_username = $('#log_username').val();
        var log_password = $('#log_password').val();
        var form = $(".log-form").val();
        if (log_password == "" || log_username == "") {
            $('.result').text(ajaxReq);
        } else {

            $.ajax(
                {
                    type: 'POST',
                    data: {
                        log_username: log_username,
                        log_password: log_password,
                        form: form
                    },
                    url: "controller",
                    success: function (result) {
                        $('#forwarddirection').val(result);
                        $('#forwardform').submit();
                    },
                    error: function (jqXHR) {
                        if (jqXHR.status == 404) {
                            $('.result').text(ajaxLoginIncor);
                        } else if (jqXHR.status == 401) {
                            $('.result').text("");
                            var forma = $('#login-form-id');
                            alert(jqXHR.responseText);
                            forma.trigger("reset");
                        }
                    }
                }
            )
        }
    });

    $('.button-register').click(function () {

        var reg_username = $('#reg_username').val();
        var reg_password = $('#password').val();
        var reg_mail = $('#reg_email').val();
        var reg_phone = $('#phone').val();
        var form = $(".reg-form").val();

        if (reg_username == "" || reg_phone == "" || reg_password == "" || reg_mail == "") {
            $('.result-reg').text(ajaxReq);

        } else {

            $.ajax(
                {
                    type: 'POST',
                    data: {
                        reg_username: reg_username,
                        reg_password: reg_password,
                        reg_mail: reg_mail,
                        reg_phone: reg_phone,
                        form: form
                    },
                    url: "controller",
                    success: function (result) {
                        var forma = $('#register-form-id');
                        $('.result-reg').text(ajaxReqSuccess);
                        forma.trigger("reset");
                    },
                    error: function (jqXHR) {
                        $('.result-reg').text(jqXHR.responseText);
                    }
                }
            )
        }
    });


    $('.updateorder').click(function (event) {

        var productId =  event.target.dataset.productIdentifier;
        var productPrice = event.target.dataset.productPrice;
        var productType = event.target.dataset.productType;
        //sometimes productId is null for some reasons
        var discount = $('.user-price-invis').text();
        var currentPrice = $('.number-price').text();
        var newPrice = productPrice - (productPrice * discount / 100) - (-currentPrice);
        $.ajax(
            {
                type: 'POST',
                data: {
                    productId:productId,
                    productType:productType,
                    form: "insert-product",
                    price: newPrice.toFixed(2),
                },
                url: "controller",
                success: function (result) {
                    var htmlCode = "<li class = 'order-li' data-value = '"+productType+":"+productId+"'>" +
                        "<span class = 'clearitem' onclick = onLiClick(this);>"+result+"</span></li>";
                    $(htmlCode).appendTo($('.ul-order'));
                    $('.number-price').text(newPrice.toFixed(2));
                    if (!($($(event.target).parent()).hasClass('active'))){
                        $($(event.target).parent()).toggleClass('active');
                    }
                    $('.order-good').text("");

                },
                error: function (jqXHR) {
                    $('.modal').toggleClass('active');
                    $('.modal-overlay').toggleClass('active');
                    $('.user_menu').toggleClass('active');
                }
            }
        )

    });



    $('.formalize-btn').on('click', function(event) {
        event.preventDefault();
        var elementsLI = document.getElementsByClassName('order-li');
        var orderPrice =  $('.number-price').text();
        var elemList = "";
        var discount = $('.user-price-invis').text();
        for(var i = 0; i <elementsLI.length ; i++){
            elemList += elementsLI[i].getAttribute('data-value') + " ";
        }
        $.ajax(
            {
                type: 'POST',
                data: {
                    order:elemList,
                    orderPrice:orderPrice,
                    form: "order-ajax",
                },
                url: "controller",
                success: function (result) {
                    $(".ul-order").empty();
                    $('.order-good').text(ajaxOrderSuccess);
                    $('.number-price').text("0.00");
                    if (discount !== 20){
                        $(".user-price-invis").text(discount++);
                    }

                    var elements = document.getElementsByClassName("goodorderdiv");
                    for (var i = 0; i < elements.length; i++) {
                        if ($(elements[i]).hasClass("active")) {
                            $(elements[i]).toggleClass('active')
                        }
                    }
                },
                error: function (jqXHR) {
                    $('.modal').toggleClass('active');
                    $('.modal-overlay').toggleClass('active');
                }
            }
        )

    });

    $('.drop-btn').on('click', function(event) {
        event.preventDefault();
        $.ajax(
            {
                type: 'POST',
                data: {
                    form: "drop-ajax",
                },
                url: "controller",
                success: function (result) {
                    $(".ul-order").empty();
                    $('.number-price').text("0.00");
                    $('.order-good').text("");

                    var elements = document.getElementsByClassName("goodorderdiv");
                    for (var i = 0; i < elements.length; i++) {
                        if ($(elements[i]).hasClass("active")) {
                            $(elements[i]).toggleClass('active')
                        }
                    }
                },
                error: function (jqXHR) {
                    $('.order-good').text(ajaxOrderError);
                }
            }
        )
    });

    $('.user-password.change-btn').on('click', function() {
        if ($("#change_password").valid()) {
            var change_password = $('#change-password').val();

            if (change_password!==""){
                $.ajax(
                    {
                        type: 'POST',
                        data: {
                            change_password: change_password,
                            form: "change_password"
                        },
                        url: "controller",
                        success: function (result) {
                            var form = $('#change_password');
                            $('.result-chg').text("Данные обновлены.");
                            $('.result-chg').text(ajaxChangeSuccess);
                            form.trigger("reset");
                        },
                        error: function (jqXHR) {
                            var form = $('#change_password');
                            $('.result-chg').text(jqXHR.responseText);
                            form.trigger("reset");
                        }
                    }
                )
            }
        }
    });

    $('.user-email.change-btn').on('click', function() {
        if ($("#change_email").valid()) {
            var change_mail = $('#change-mail').val();

            if (change_mail !== "") {
                $.ajax(
                    {
                        type: 'POST',
                        data: {
                            change_mail: change_mail,
                            form: "change_mail"
                        },
                        url: "controller",
                        success: function (result) {
                            var form = $('#change_email');
                            $('.result-chg').text(ajaxChangeSuccess);
                            $(".current-mail").text(change_mail);
                            form.trigger("reset");
                        },
                        error: function (jqXHR) {
                            var form = $('#change_email');
                            $('.result-chg').text(jqXHR.responseText);
                            form.trigger("reset");
                        }
                    }
                )

            }
        }
    });

    $('.user-phone.change-btn').on('click', function() {
        if ($("#change_phone").valid()) {
            var change_phone = $('#change-phone').val();

            if (change_phone !== "") {
                $.ajax(
                    {
                        type: 'POST',
                        data: {
                            change_phone: change_phone,
                            form: "change_phone"
                        },
                        url: "controller",
                        success: function (result) {
                            var form = $('#change_password');
                            $('.result-chg').text(ajaxChangeSuccess);
                            $(".current-phone").text(change_phone);
                            form.trigger("reset");
                        },
                        error: function (jqXHR) {
                            var form = $('#change_password');
                            $('.result-chg').text(jqXHR.responseText);
                            form.trigger("reset");
                        }
                    }
                )

            }
        }
    });

});

