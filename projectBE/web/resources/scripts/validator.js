function resetData(){
	$('.result-chg').text('');
}

jQuery (function($){

	$('form[id="register-form-id"]').validate({
		rules: {
			name: {
				required: true,
				loginValid: true,
				minlength: 5,
				maxlength: 15,
			},
			email: {
				required: true,
				email: false,
				emailValid: true,
				maxlength: 35,
			},
			password: {
				passwordValid: true,
				required: true,
				minlength: 6,
				maxlength: 20,
			},
			confirm_password: {
				required: true,
				minlength: 6,
				equalTo: "#password",
			},
			phone: {
				required: true,
				telValid: true,
			}
		},
		messages: {
			name: {
				required:validRegReq,
				minlength:validReqNameMin,
				maxlength:validRegNameMax
			},
			email:{
				required: validRegReq,
				maxlength:validReqMailMax
			},
			password: {
				required:validRegReq,
				maxlength:validRegPassMax,
				minlength: validRegPassMin
			},
			confirm_password: {
				required: validRegReq,
				minlength: validRegPassMin,
				equalTo: validRegRepEqu
			},
			phone: {
				required: validRegReq
			}

		},
		submitHandler: function(form) {
			form.submit();
		}
	});

	$('form[id="login-form-id"]').validate({
		rules: {
			name: {
				required: true,
				minlength: 5,
			},
			password: {
				required: true,
				minlength: 6,
			}
		},
		messages: {
			name: {
				required:validLogReq,
				minlength:validLogLen
			},
			password: {
				required:validLogPasReq,
				minlength: validLogPasMin
			}
		},
	});

	$('form[id="change_phone"]').validate({
		rules: {
			newphone: {
				required: false,
				telValid: true,
			}
		},

		submitHandler: function(form) {
			form.submit();
		}
	});

	$('form[id="change_email"]').validate({
		rules: {
			newmail: {
				required: false,
				email: false,
				emailValid: true,
				maxlength: 35,
			},

		},
		messages: {
			newmail:{
				maxlength: validChangeMailMax
			},

		},
		submitHandler: function(form) {
			form.submit();
		}
	});


	$('form[id="change_password"]').validate({
		rules: {
			newpassword: {
				passwordValid: true,
				required: false,
				minlength: 6,
				maxlength: 20,
			}
		},
		messages: {
			newpassword: {
				maxlength:validChangePassMax,
				minlength: validChangePassMin
			}
		},
		submitHandler: function(form) {
			form.submit();
		}
	});

	jQuery.validator.addMethod("passwordValid", function(value, element){
		if(value != ""){
			return /(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])[A-Za-z0-9]/.test(value);
		}

		return true;

	}, validFuncPass);
	jQuery.validator.addMethod("emailValid", function(value, element){
		if(value != ""){
			return /^.+@.+\..+$/.test(value);
		}

		return true;
	}, validFuncMail);
	jQuery.validator.addMethod("telValid", function(value, element){
		if(value != ""){
			return /^(\+)?(375)\d{9}$/.test(value);
		}

		return true;

	}, validFuncPhone);
	jQuery.validator.addMethod("loginValid", function(value, element){
		return /^([A-Za-z])[\w+]/.test(value);
	}, validFuncName);


});
