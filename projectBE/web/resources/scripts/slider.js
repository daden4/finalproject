$(document).ready(function(){
    $('.main-content').slick();
    $('.pizza-carousel').slick({
        nextArrow:"<div class='carousel-right'><img src='resources/images/second-right.png' alt='Вправо'></div>",
        prevArrow:"<div class='carousel-left'><img src='resources/images/second-left.png' alt='Вправо'></div>",
        dots: true,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3
    });

    $('.drink-carousel').slick({
        centerMode: true,
        infinite: true,
        slidesToShow: 3,
        speed: 500,
        variableWidth: false,
        nextArrow:"<div class='carousel-right'><img src='resources/images/right.png' alt='Вправо'></div>",
        prevArrow:"<div class='carousel-left'><img src='resources/images/left.png' alt='Влево'></div>",
    });
});
