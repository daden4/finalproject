<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 24.07.2019
  Time: 21:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="pagecontent" prefix="userpage.">

    <c:if test="${sessionScope.client == null}">
        <jsp:forward page="index.jsp"/>
    </c:if>

<!DOCTYPE html>
<html lang="${sessionScope.locale}">
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="title"/></title>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" href="resources/styles/account.css" type="text/css">
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="resources/scripts/jsLoc.js" type="text/javascript"></script>
    <script src="resources/scripts/ajax.js" type="text/javascript"></script>
    <script src="resources/scripts/validator.js" type="text/javascript"></script>
</head>

<body>
<c:set var="locale" value="${sessionScope.locale}"/>
<c:if test="${sessionScope.locale == null}">
    <c:set var="locale" value="ru"/>
</c:if>

<span id="pageLanguage" hidden>${locale}</span>

<div class="main-overlay"></div>
<div class="bg-image">
    <div class="header-back"></div>
</div>

<header>

    <div class="logo">
        <img src="resources/images/logo.png" alt="Логотип">
    </div>

    <div class="show-menu-btn">
        <i class="fa fa-bars"></i>
    </div>


    <div id = "menu-scroll" class="menu visible-lg">
        <ul>
            <li><span class="login"></span></li>
            <li><span><fmt:message key="userdiscount"/> <span class="discount"></span></span></li>
            <li><span><fmt:message key="userphone"/> <span class="current-phone"></span></span></li>
            <li><span><fmt:message key="usermail"/> <span class="current-mail"></span></span></li>
            <li><span><fmt:message key="orderdate"/> <span class="current-date"></span></span></li>

        </ul>
    </div>

</header>
<div class="container">

    <form id="change_phone" novalidate="" accept-charset="utf-8">
        <p><fmt:message key="userchangephone"/></p>
        <input type="text" name="newphone" maxlength="13" id="change-phone" onclick="resetData()">
        <input type="button" value="<fmt:message key="btnphone"/>" name="phone_button" class="user-phone change-btn" >
        <input type="hidden" name="form" value="change_phone" class="change-form"/>
    </form>

    <form id="change_email" novalidate="" accept-charset="utf-8">
        <p><fmt:message key="userchangemail"/></p>
        <input type="text" name="newmail" id="change-mail" onclick="resetData()">
        <input type="button" value="<fmt:message key="btnmail"/>" name="mail_button" class="user-email change-btn" >
        <input type="hidden" name="form" value="change_email" class="change-form"/>
    </form>

    <form id="change_password" novalidate="" accept-charset="utf-8">
        <p><fmt:message key="userchangepass"/></p>
        <input type="password" name="newpassword" id="change-password" onclick="resetData()">
        <input type="button" value="<fmt:message key="btnpass"/>" name="password_button" class="user-password change-btn">
        <input type="hidden" name="form" value="change_password" class="change-form"/>
    </form>

    <br>
    <span class="result-chg"></span>
    <input type="button" value="<fmt:message key="forwardmain"/>" name="main-page-btn" class="main-page-btn">

    <p><fmt:message key="orderstory"/></p>
    <div class="order-history"></div>
</div>
<script>
    $(function () {
        <c:set var="user" value="${sessionScope.client}"/>

        $(".login").text('${user.name}');
        $(".discount").text('${user.discount}%');
        $(".current-phone").text('${user.phone}');
        $(".current-mail").text('${user.email}');
        $(".current-date").text('${user.orderDate}');

        $('${sessionScope.order}').appendTo($('.order-history'));

        $('.main-page-btn').on('click', function(event) {
            event.preventDefault();
            location.replace("index.jsp");
        });

    });

</script>
</body>
</html>
</fmt:bundle>