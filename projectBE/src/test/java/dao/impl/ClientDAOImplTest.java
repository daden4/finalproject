package dao.impl;

import dao.ClientDAO;
import dao.exception.BannedPersonException;
import dao.factory.impl.DAOFactoryImpl;
import entity.Client;
import entity.UserRole;
import helper.ServletContextConnector;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;

import static org.testng.Assert.*;

public class ClientDAOImplTest {
    @BeforeClass
    public static void setUpClass(){
        ServletContextConnector.setUpServletContext();
    }

    @Test
    public void testGetAllUserInformation() {
        ClientDAO clientDAO = DAOFactoryImpl.getInstance().getClientDAO();
        String actualString = clientDAO.getAllUserInformation("Daden");
        String expectedString = "Daden +375447167573 daden225@mail.ru 4 2019-08-06 1 admin";
        assertEquals(actualString, expectedString);
    }

    @Test
    public void testSignIn() {
        try {
            ClientDAO clientDAO = DAOFactoryImpl.getInstance().getClientDAO();
            Client client = new Client();
            client.setName("Daden");
            client.setPassword("111111");
            boolean actualResult = clientDAO.signIn(client);
            boolean expectedResult = true;
            assertEquals(actualResult, expectedResult);
        } catch (BannedPersonException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetClientList() {
        ClientDAO clientDAO = DAOFactoryImpl.getInstance().getClientDAO();
        ArrayList<Client> clients = clientDAO.getClientList();
        Client actualClient = clients.get(0);
        Client expectedClient = new Client();
        expectedClient.setId(1);
        expectedClient.setName("Daden");
        expectedClient.setPhone("+375447167573");
        expectedClient.setEmail("daden225@mail.ru");
        expectedClient.setPassword("111111");
        expectedClient.setOrderDate("2019-08-06");
        expectedClient.setDiscount(4);
        expectedClient.setStatus(UserRole.ADMIN);
        assertEquals(actualClient, expectedClient);
    }
}