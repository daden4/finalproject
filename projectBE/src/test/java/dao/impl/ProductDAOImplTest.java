package dao.impl;

import dao.ProductDAO;
import dao.factory.impl.DAOFactoryImpl;
import entity.Product;
import entity.ProductType;
import helper.ServletContextConnector;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static org.testng.Assert.*;

public class ProductDAOImplTest {
    @BeforeClass
    public static void setUpClass(){
        ServletContextConnector.setUpServletContext();
    }

    @Test
    public void testGetProductList() {
        ProductDAO productDAO = DAOFactoryImpl.getInstance().getProductDAO();
        ArrayList<Product> products = productDAO.getProductList();
        Product actualProduct = products.get(0);
        Product expectedProduct = new Product();
        expectedProduct.setProductID(1);
        expectedProduct.setProductType(ProductType.PIZZA);
        expectedProduct.setProductName("Неаполитана");
        expectedProduct.setSize("26.00");
        expectedProduct.setPrice("14.30");
        assertEquals(actualProduct, expectedProduct);
    }
}