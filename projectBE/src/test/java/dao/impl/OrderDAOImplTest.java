package dao.impl;

import dao.OrderDAO;
import dao.factory.impl.DAOFactoryImpl;
import entity.Order;
import helper.ServletContextConnector;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static org.testng.Assert.*;

public class OrderDAOImplTest {
    @BeforeClass
    public static void setUpClass(){
        ServletContextConnector.setUpServletContext();
    }

    @Test
    public void testGetOrderList() {
        OrderDAO orderDAO = DAOFactoryImpl.getInstance().getOrderDAO();
        ArrayList<Order> orders = orderDAO.getOrderList();
        Order actualOrder = orders.get(0);
        String actualPrice = actualOrder.getOrderPrice();
        String expectedPrice = "10.73";
        assertEquals(actualPrice, expectedPrice);
    }
}