package dao.impl;

import dao.BlockDAO;
import dao.factory.DAOFactory;
import dao.factory.impl.DAOFactoryImpl;
import entity.BlockReason;
import helper.ServletContextConnector;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static org.testng.Assert.*;

public class BlockDAOImplTest {
    @BeforeClass
    public static void setUpClass(){
        ServletContextConnector.setUpServletContext();
    }

    @Test
    public void testGetBlockReasonList() {
        BlockDAO blockDAO = DAOFactoryImpl.getInstance().getBlockDAO();
        ArrayList<BlockReason> actualList = blockDAO.getBlockReasonList();
        BlockReason actualReason = actualList.get(0);
        BlockReason expectedReason = new BlockReason();
        expectedReason.setId(1);
        expectedReason.setName("Отказ уплаты");
        expectedReason.setDescription("При доставке клиент отказался платить(блок телефона, адреса, email).");
        assertEquals(actualReason, expectedReason);
    }
}