package utility;

import org.testng.annotations.Test;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import static org.testng.Assert.*;

public class PasswordEncrypterTest {

    @Test
    public void testEncrypt() {
        PasswordEncrypter passwordEncrypter = new PasswordEncrypter();
        try {
            System.out.println(passwordEncrypter.encrypt("password","111111"));
        } catch (NoSuchAlgorithmException | InvalidKeySpecException
                | NoSuchPaddingException | InvalidKeyException |
                InvalidAlgorithmParameterException | BadPaddingException |
                IllegalBlockSizeException | IOException e) {
            e.printStackTrace();
        }
    }
}