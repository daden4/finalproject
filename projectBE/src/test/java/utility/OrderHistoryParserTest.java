package utility;

import entity.Client;
import entity.Order;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static org.testng.Assert.*;

public class OrderHistoryParserTest {

    @Test
    public void testParse() {
        OrderHistoryParser orderHistoryParser = new OrderHistoryParser();
        Order order = new Order();
        Client client = new Client();
        client.setName("Vasya");
        order.setOrderId(23);
        order.setOrderDate("15");
        order.setOrderPrice("100");
        order.setClient(client);

        String actualString = orderHistoryParser.parse(new ArrayList<Order>(){{add(order);}}, "Vasya", "ru");
        String expectedString = "<p>Идентификатор заказа: 23. Дата заказа:  15. Цена заказа с учетом скидки:  100 р.</p>";

        assertEquals(actualString, expectedString);
    }
}