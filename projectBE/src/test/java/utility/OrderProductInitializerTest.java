package utility;

import entity.Order;
import entity.Product;
import entity.ProductType;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static org.testng.Assert.*;

public class OrderProductInitializerTest {

    @Test
    public void testInitProduct() {
        OrderProductInitializer orderProductInitializer = new OrderProductInitializer();
        String orderList = "drink:1";
        Order order = new Order();
        orderProductInitializer.initProduct(orderList, order);

        ArrayList<Product> actualList = order.getProductList();
        ArrayList<Product> expectedList = new ArrayList<>();
        Product product = new Product();
        product.setQuantity(1L);
        product.setProductType(ProductType.DRINK);
        product.setProductID(1);
        expectedList.add(product);
        assertEquals(actualList, expectedList);

    }
}