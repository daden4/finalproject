package utility;

import entity.Client;
import entity.UserRole;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class ClientParserTest {

    @Test
    public void testParse() {
        ClientParser clientParser = new ClientParser();
        Client expectedClient = new Client();
        expectedClient.setId(2);
        expectedClient.setName("Xxx");
        expectedClient.setPhone("1");
        expectedClient.setEmail("2");
        expectedClient.setOrderDate("4");
        expectedClient.setDiscount(3);
        expectedClient.setStatus(UserRole.USER);

        Client actualClient = clientParser.parse("Xxx 1 2 3 4 2 user");

        assertEquals(actualClient, expectedClient);
    }
}