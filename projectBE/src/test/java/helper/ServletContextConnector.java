package helper;

import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ServletContextConnector {
    private final static String USER = "user";
    private final static String PASSWORD = "password";
    private final static String URL = "url";
    private final static String BIND_URL = "bindUrl";
    private final static String PATH_TO_PROPERTIES
            = "target\\test-classes\\dataBaseConfiguration.properties";

    private final static String JAVA = "java:";
    private final static String COMP = "comp";
    private final static String ENV = "env";
    private final static String JDBC = "jdbc";

    private final static String NAMING_FACTORY_INITIAL
            = "org.apache.naming.java.javaURLContextFactory";
    private final static String NAMING_FACTORY_URL = "org.apache.naming";

    public static void setUpServletContext() {
        try {
            System.setProperty(Context.INITIAL_CONTEXT_FACTORY, NAMING_FACTORY_INITIAL);
            System.setProperty(Context.URL_PKG_PREFIXES, NAMING_FACTORY_URL);
            InitialContext context = new InitialContext();
            context.createSubcontext(JAVA);
            context.createSubcontext(JAVA + COMP);
            context.createSubcontext(JAVA + COMP + "/" + ENV);
            context.createSubcontext(JAVA + COMP + "/" + ENV + "/" + JDBC);
            Properties properties = new Properties();
            FileInputStream inputStream = new FileInputStream(PATH_TO_PROPERTIES);
            properties.load(inputStream);
            MysqlConnectionPoolDataSource mysqlConnection = new MysqlConnectionPoolDataSource();
            mysqlConnection
                    .setUser(properties.getProperty(USER));
            mysqlConnection
                    .setPassword(properties.getProperty(PASSWORD));
            mysqlConnection
                    .setURL(properties.getProperty(URL));
            context.bind(properties.getProperty(BIND_URL), mysqlConnection);
        } catch (NamingException | IOException e) {
            System.err.println(e);
        }
    }
}
