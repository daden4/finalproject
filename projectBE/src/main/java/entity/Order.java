package entity;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Represent {@link Client} order
 */

public class Order {
    private Client client;
    private String orderDate;
    private String orderPrice;
    private int orderId;

    private ArrayList<Product> productList = new ArrayList<>();

    public ArrayList<Product> getProductList() {
        return productList;
    }

    public void setProductList(ArrayList<Product> productList) {
        this.productList = productList;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(String orderPrice) {
        this.orderPrice = orderPrice;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return orderId == order.orderId &&
                Objects.equals(client, order.client) &&
                Objects.equals(orderDate, order.orderDate) &&
                Objects.equals(orderPrice, order.orderPrice) &&
                Objects.equals(productList, order.productList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(client, orderDate, orderPrice, orderId, productList);
    }

    @Override
    public String toString() {
        return "Order{" +
                "client=" + client +
                ", orderDate='" + orderDate + '\'' +
                ", orderPrice='" + orderPrice + '\'' +
                ", orderId=" + orderId +
                ", productList=" + productList +
                '}';
    }
}
