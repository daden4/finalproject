package entity;

/**
 * Enumeration for type of product in pizzeria
 * menu
 */
public enum ProductType {
    PIZZA, DRINK, SALAD;

}
