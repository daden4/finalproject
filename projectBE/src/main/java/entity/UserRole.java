package entity;

/**
 * Enumeration for identify user role
 */
public enum UserRole {
    ADMIN, USER;
}
