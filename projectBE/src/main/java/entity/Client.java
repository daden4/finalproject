package entity;

import java.util.Objects;

/**
 * Represent client of application
 */
public class Client {
    private int id;
    private String name;
    private String phone;
    private String email;
    private String password;
    private String orderDate;
    private int discount;
    private UserRole status;
    private String banReason;

    public Client(String name) {
        this.name = name;
    }

    public Client() {
    }

    public String getBanReason() {
        return banReason;
    }

    public void setBanReason(String banReason) {
        this.banReason = banReason;
    }

    public UserRole getStatus() {
        return status;
    }

    public void setStatus(UserRole status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return id == client.id &&
                discount == client.discount &&
                Objects.equals(name, client.name) &&
                Objects.equals(phone, client.phone) &&
                Objects.equals(email, client.email) &&
                Objects.equals(password, client.password) &&
                Objects.equals(orderDate, client.orderDate) &&
                status == client.status &&
                Objects.equals(banReason, client.banReason);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, phone, email, password, orderDate, discount, status, banReason);
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", orderDate='" + orderDate + '\'' +
                ", discount=" + discount +
                ", status=" + status +
                ", banReason='" + banReason + '\'' +
                '}';
    }
}
