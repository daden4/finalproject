package entity;

import java.util.Objects;

/**
 * Represent {@link Order} products and
 * products from table in database
 */
public class Product {
    private ProductType productType;
    private int productID;
    private Long quantity;
    private String productName;
    private String size;
    private  String price;
    private String enName;

    public Product() {
    }

    public Product(ProductType productType, Long quantity, String productName, String price, String enName) {
        this.productType = productType;
        this.quantity = quantity;
        this.productName = productName;
        this.price = price;
        this.enName = enName;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return productID == product.productID &&
                productType == product.productType &&
                Objects.equals(quantity, product.quantity) &&
                Objects.equals(productName, product.productName) &&
                Objects.equals(size, product.size) &&
                Objects.equals(price, product.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productType, productID, quantity, productName, size, price);
    }

    @Override
    public String toString() {
        return "Product{" +
                "productType=" + productType +
                ", productID=" + productID +
                ", quantity=" + quantity +
                ", productName='" + productName + '\'' +
                ", size=" + size +
                ", price='" + price + '\'' +
                '}';
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    public String getEnName() {
        return enName;
    }
}
