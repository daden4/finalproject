package filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import java.io.IOException;

/**
 * Filter for set charset to UTF-8
 */
public class CharsetFilter implements Filter {
    private static final Logger logger = LogManager.getLogger(CharsetFilter.class);
    private String encoding;
    private ServletContext context;

    @Override
    public void init(FilterConfig filterConfig)  {
        encoding = filterConfig.getInitParameter("characterEncoding");
        context = filterConfig.getServletContext();
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        servletRequest.setCharacterEncoding(encoding);
        servletResponse.setCharacterEncoding(encoding);

        context.log("Character was set.");
        logger.info("Character was set.");

        filterChain.doFilter(servletRequest,servletResponse);
    }

    @Override
    public void destroy() {
    }
}
