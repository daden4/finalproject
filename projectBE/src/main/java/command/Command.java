package command;

import command.exception.CommandException;

/**
 * Main interface of command which
 * implemented each command class
 */

public interface Command {
    /**
     *Execute command
     * @throws CommandException if execution failed
     */
    void execute() throws CommandException;
}