package command.impl;


import command.Command;
import entity.Product;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;


/**
 * Command for reset shopping cart and it session
 */
public class DropOrderSessionCommand implements Command {
    private HttpServletRequest request;
    private HttpServletResponse response;
    public DropOrderSessionCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    @Override
    public void execute() {
        request.getSession().setAttribute("ajaxOrder",new ArrayList<Product>());
        request.getSession().setAttribute("orderPrice","0.00");
    }
}