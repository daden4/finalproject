package command.impl;

import command.Command;
import dao.exception.BannedPersonException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.GuestService;
import service.exception.InvalidLogInException;
import service.factory.impl.ServiceFactoryImpl;
import service.impl.GuestServiceImpl;
import utility.AjaxResponseSetter;
import utility.LogInSessionInitializer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Guest command for logIn
 */

public class LogInCommand implements Command {
    private static final Logger logger = LogManager.getLogger(LogInCommand.class);
    private HttpServletRequest request;
    private HttpServletResponse response;
    public LogInCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    @Override
    public void execute() {
        try {
            String log_username = request.getParameter("log_username");
            String log_password = request.getParameter("log_password");
            GuestService service = ServiceFactoryImpl.getInstance().getGuestService();
            service.signIn(log_username, log_password);
            LogInSessionInitializer sessionInitializer = new LogInSessionInitializer();
            sessionInitializer.initialize(request,response);
        } catch (BannedPersonException e) {
            logger.warn(e);
            AjaxResponseSetter responseSetter = new AjaxResponseSetter();
            responseSetter.setResponse(response, e, 401);
        } catch (InvalidLogInException e) {
            logger.info(e);
            AjaxResponseSetter responseSetter = new AjaxResponseSetter();
            responseSetter.setSimpleResponse(response, 404);
        }
    }
}