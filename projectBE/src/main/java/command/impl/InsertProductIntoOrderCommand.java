package command.impl;

import command.Command;
import entity.Product;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.UserService;
import service.exception.InsertProductException;
import service.factory.impl.ServiceFactoryImpl;
import service.impl.UserServiceImpl;
import utility.AjaxResponseSetter;
import utility.ProductInfoHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

/**
 * Command for add product into shopping cart and session with ajax
 */

public class InsertProductIntoOrderCommand implements Command {
    private static final Logger logger = LogManager.getLogger(InsertProductIntoOrderCommand.class);
    private HttpServletRequest request;
    private HttpServletResponse response;

    public InsertProductIntoOrderCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    @Override
    public void execute() {
        try {
            if (request.getSession().getAttribute("client") == null) {
                AjaxResponseSetter responseSetter = new AjaxResponseSetter();
                responseSetter.setSimpleResponse(response, 420);
            } else {
                String productId = request.getParameter("productId");
                String productPrice = request.getParameter("price");
                String productType = request.getParameter("productType");
                UserService service = ServiceFactoryImpl.getInstance().getUserService();
                String locale = (String) request.getSession().getAttribute("locale");
                Product product = service.addProduct(productId, productType);
                ArrayList<Product> products = (ArrayList<Product>) request.getSession().getAttribute("ajaxOrder");
                products.add(product);
                request.getSession().setAttribute("orderPrice", productPrice);
                request.getSession().setAttribute("ajaxOrder", products);
                ProductInfoHandler productInfoHandler = new ProductInfoHandler();
                String productInfo = productInfoHandler.initialize(product, locale);

                AjaxResponseSetter ajaxResponseSetter = new AjaxResponseSetter();
                ajaxResponseSetter.setPositiveResponse (response, productInfo, 220);
            }
        } catch (InsertProductException e) {
            logger.error(e);
            AjaxResponseSetter ajaxResponseSetter = new AjaxResponseSetter();
            ajaxResponseSetter.setResponse (response, e, 490);
        }
    }
}