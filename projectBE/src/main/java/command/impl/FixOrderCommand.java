package command.impl;

import command.Command;
import dao.OrderDAO;
import dao.factory.impl.DAOFactoryImpl;
import entity.Client;
import entity.Order;
import entity.Product;
import service.UserService;
import service.factory.impl.ServiceFactoryImpl;
import service.impl.UserServiceImpl;
import utility.OrderFixSessionInitializer;
import utility.OrderHistoryParser;
import utility.OrderProductInitializer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
/**
 * Command for fixing order into database
 */

public class FixOrderCommand implements Command {
    private HttpServletRequest request;
    private HttpServletResponse response;
    public FixOrderCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    @Override
    public void execute() {
        String orderList = request.getParameter("order");
        String orderPrice = request.getParameter("orderPrice");
        Client client = (Client) request.getSession().getAttribute("client");
        UserService service = ServiceFactoryImpl.getInstance().getUserService();

        Order order = service.fixOrder(orderList, orderPrice, client);
        request.setAttribute("orderRequest", order);
        OrderFixSessionInitializer orderFixSessionInitializer = new OrderFixSessionInitializer();
        orderFixSessionInitializer.initialize(request, response);
    }
}
