package command.impl;

import command.Command;
import entity.Product;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

/**
 * User command for remove product from shopping cart and session
 * with ajax
 */

public class RemoveOrderPositionCommand implements Command {
    private HttpServletRequest request;
    private HttpServletResponse response;
    public RemoveOrderPositionCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    @Override
    public void execute() {
        String orderPrice = request.getParameter("orderPrice");
        String index = request.getParameter("index");
        ArrayList<Product> products = (ArrayList<Product>) request.getSession().getAttribute("ajaxOrder");
        products.remove(Integer.parseInt(index));
        request.getSession().setAttribute("orderPrice",orderPrice);
        request.getSession().setAttribute("ajaxOrder",products);
    }
}