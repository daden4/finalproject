package command.impl;

import command.Command;
import command.exception.CommandException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * User command for log out (destroy session)
 */

public class LogOutCommand implements Command {
    private static final Logger logger = LogManager.getLogger(LogOutCommand.class);
    private HttpServletRequest request;
    private HttpServletResponse response;
    public LogOutCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    @Override
    public void execute() throws CommandException {
        try {
            request.getSession().invalidate();
            request.getRequestDispatcher("index.jsp").forward(request,response);
        } catch ( IOException | ServletException e) {
            logger.error(e);
            throw new CommandException();
        }
    }
}