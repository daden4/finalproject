package command.impl;

import command.Command;
import command.exception.CommandException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.AdminService;
import service.factory.impl.ServiceFactoryImpl;
import utility.AdminTypeGetter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Admin command for select all position of some table
 */

public class ManageAdminRequestCommand implements Command {
    private static final Logger logger = LogManager.getLogger(ManageAdminRequestCommand.class);
    private HttpServletRequest request;
    private HttpServletResponse response;
    public ManageAdminRequestCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    @Override
    public void execute() throws CommandException {
        try {
            String productSelect = request.getParameter("product_sel_btn");
            String clientSelect = request.getParameter("client_sel_btn");
            String orderSelect = request.getParameter("order_sel_btn");
            String reasonSelect = request.getParameter("reason_sel_btn");

            AdminService adminService = ServiceFactoryImpl.getInstance().getAdminService();
            ArrayList adminInfo = adminService.getRequiredList(productSelect, clientSelect, orderSelect, reasonSelect);

            AdminTypeGetter adminTypeGetter = new AdminTypeGetter();
            String buttonType = adminTypeGetter.getType(productSelect, clientSelect, orderSelect, reasonSelect);

            request.setAttribute("adminInfo",adminInfo);
            request.setAttribute("buttonType", buttonType);
            request.getRequestDispatcher("/WEB-INF/jsp/admin/admin.jsp").forward(request,response);
        } catch (ServletException | IOException  e) {
            logger.error(e);
            throw new CommandException();
        }

    }
}
