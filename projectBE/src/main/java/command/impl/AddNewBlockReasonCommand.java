package command.impl;

import command.Command;
import command.exception.CommandException;
import controller.Controller;
import entity.BlockReason;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.AdminService;
import service.exception.AdminServiceException;
import service.factory.impl.ServiceFactoryImpl;
import service.impl.AdminServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;


/**
 * Admin command for create block reasons
 */
public class AddNewBlockReasonCommand implements Command {
    private HttpServletRequest request;
    private HttpServletResponse response;
    private static final Logger logger = LogManager.getLogger(AddNewBlockReasonCommand.class);

    public AddNewBlockReasonCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    @Override
    public void execute() throws CommandException {
        try {
            String reasonName = request.getParameter("reason_name");
            String reasonDescription = request.getParameter("reason_description");
            AdminService adminService = ServiceFactoryImpl.getInstance().getAdminService();
            ArrayList<BlockReason> blockReasons = adminService.setNewReason(reasonName, reasonDescription);
            request.setAttribute("adminInfo",blockReasons);
            request.setAttribute("buttonType", "blockreason");
            request.getRequestDispatcher("/WEB-INF/jsp/admin/admin.jsp").forward(request,response);
        } catch (ServletException | IOException e) {
            logger.error(e);
            throw new CommandException();
        }
    }
}
