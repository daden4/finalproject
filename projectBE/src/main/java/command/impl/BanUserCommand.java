package command.impl;

import command.Command;
import command.exception.CommandException;
import entity.Client;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.AdminService;
import service.exception.AdminServiceException;
import service.factory.ServiceFactory;
import service.factory.impl.ServiceFactoryImpl;
import service.impl.AdminServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;


/**
 * Admin command for block users
 */
public class BanUserCommand implements Command {
    private static final Logger logger = LogManager.getLogger(BanUserCommand.class);
    private HttpServletRequest request;
    private HttpServletResponse response;
    public BanUserCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    @Override
    public void execute() throws CommandException {
        try {
            String[] blockID = request.getParameterValues("block_id[]");
            String[] userID = request.getParameterValues("users_id[]");
            AdminService adminService = ServiceFactoryImpl.getInstance().getAdminService();
            ArrayList<Client> clients = adminService.setBan(blockID, userID);
            request.setAttribute("adminInfo",clients);
            request.setAttribute("buttonType", "client");
            request.getRequestDispatcher("/WEB-INF/jsp/admin/admin.jsp").forward(request,response);
        } catch (ServletException | IOException e) {
            logger.error(e);
            throw  new CommandException();
        }
    }
}
