package command.impl;

import command.Command;
import dao.exception.SameMailException;
import dao.exception.SamePhoneException;
import entity.Client;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.UserService;
import service.exception.UserServiceException;
import service.factory.impl.ServiceFactoryImpl;
import service.impl.UserServiceImpl;
import utility.AjaxResponseSetter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * User command for change it phone
 */
public class ChangePhoneCommand implements Command {
    private static final Logger logger = LogManager.getLogger(ChangePhoneCommand.class);
    private HttpServletRequest request;
    private HttpServletResponse response;
    public ChangePhoneCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    @Override
    public void execute() {
        try {
            String change_phone = request.getParameter("change_phone");
            UserService service = ServiceFactoryImpl.getInstance().getUserService();
            Client client = (Client) request.getSession().getAttribute("client");
            service.changePhone(client, change_phone);
        } catch (SamePhoneException | UserServiceException | SameMailException e) {
            logger.info(e);
            AjaxResponseSetter ajaxResponseSetter = new AjaxResponseSetter();
            ajaxResponseSetter.setResponse(response, e, 410);
        }
    }
}
