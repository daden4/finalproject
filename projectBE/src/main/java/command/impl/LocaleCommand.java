package command.impl;

import command.Command;
import command.exception.CommandException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import utility.LanguageSessionInitializer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Command for change locale
 */

public class LocaleCommand implements Command {
    private static final Logger logger = LogManager.getLogger(LocaleCommand.class);
    private HttpServletRequest request;
    private HttpServletResponse response;

    public LocaleCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    @Override
    public void execute() throws CommandException {
        try {
            String ruLanguage = request.getParameter("ruLanguage");
            String enLanguage  = request.getParameter("enLanguage");
            HttpSession session = request.getSession();
            if (ruLanguage != null){
                session.setAttribute("locale","ru");
            }
            if (enLanguage != null){
                session.setAttribute("locale","en");
            }
            LanguageSessionInitializer languageSessionInitializer = new LanguageSessionInitializer();
            languageSessionInitializer.initialize(request, response);
            request.getRequestDispatcher("index.jsp").forward(request,response);
        } catch (ServletException | IOException e) {
            logger.error(e);
            throw new CommandException();
        }
    }
}
