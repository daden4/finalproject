package command.impl;

import command.Command;
import dao.exception.SameMailException;
import dao.exception.SameNameException;
import dao.exception.SamePhoneException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.GuestService;
import service.exception.ServiceException;
import service.factory.impl.ServiceFactoryImpl;
import service.impl.GuestServiceImpl;
import utility.AjaxResponseSetter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Guest command for register new users
 */

public class RegisterCommand implements Command {
    private static final Logger logger = LogManager.getLogger(RegisterCommand.class);
    private HttpServletRequest request;
    private HttpServletResponse response;
    public RegisterCommand(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    @Override
    public void execute(){
        try {
            String name = request.getParameter("reg_username");
            String email = request.getParameter("reg_mail");
            String phone = request.getParameter("reg_phone");
            String password = request.getParameter("reg_password");
            GuestService service = ServiceFactoryImpl.getInstance().getGuestService();
            service.signUp(name, email, phone, password);
        } catch (SamePhoneException | SameMailException | SameNameException | ServiceException e) {
            logger.info(e);
            AjaxResponseSetter responseSetter = new AjaxResponseSetter();
            responseSetter.setResponse(response, e, 400);
        }
    }
}