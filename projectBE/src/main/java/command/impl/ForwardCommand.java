package command.impl;

import command.Command;
import command.exception.CommandException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ForwardCommand implements Command {
    private HttpServletRequest request;
    private HttpServletResponse response;
    private static final Logger logger = LogManager.getLogger(ForwardCommand.class);

    public ForwardCommand(HttpServletRequest request, HttpServletResponse response) {
        this.response = response ;
        this.request = request ;
    }

    @Override
    public void execute() throws CommandException {
        try {
            String forwardDirection = request.getParameter("direction");
            request.getRequestDispatcher(forwardDirection).forward(request, response);
        } catch (ServletException | IOException e) {
            logger.error(e);
            throw new CommandException();
        }
    }
}
