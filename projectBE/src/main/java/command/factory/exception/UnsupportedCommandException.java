package command.factory.exception;

/**
 * Exception for not define form value
 */

public class UnsupportedCommandException extends Exception {
    public UnsupportedCommandException() {
        super();
    }

    public UnsupportedCommandException(String message) {
        super(message);
    }

    public UnsupportedCommandException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnsupportedCommandException(Throwable cause) {
        super(cause);
    }

    protected UnsupportedCommandException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
