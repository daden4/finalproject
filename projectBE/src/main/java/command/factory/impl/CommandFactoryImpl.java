package command.factory.impl;

import command.Command;
import command.factory.CommandFactory;
import command.factory.exception.UnsupportedCommandException;
import command.impl.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Realization of {@link CommandFactory}
 */
public class CommandFactoryImpl implements CommandFactory {
    private static final CommandFactoryImpl INSTANCE = new CommandFactoryImpl();

    private CommandFactoryImpl() {
    }

    public static CommandFactoryImpl getInstance() {
        return INSTANCE;
    }

    @Override
    public Command getCommand(String command, HttpServletRequest request, HttpServletResponse response) throws UnsupportedCommandException {
        switch (command) {
            case "back":
                return new LogOutCommand(request, response);
            case "drop-ajax":
                return new DropOrderSessionCommand(request, response);
            case "ul-ajax":
                return new RemoveOrderPositionCommand(request, response);
            case "user_form":
                return new BanUserCommand(request, response);
            case "block_form":
                return new AddNewBlockReasonCommand(request, response);
            case "admin_form":
                return new ManageAdminRequestCommand(request, response);
            case "insert-product":
                return new InsertProductIntoOrderCommand(request, response);
            case "order-ajax":
                return new FixOrderCommand(request, response);
            case "register":
                return new RegisterCommand(request, response);
            case "login":
                return new LogInCommand(request, response);
            case "change_password":
                return new ChangePasswordCommand(request, response);
            case "change_mail":
                return new ChangeEmailCommand(request, response);
            case "change_phone":
                return new ChangePhoneCommand(request, response);
            case "locale_form":
                return new LocaleCommand(request, response);
            case "sendforward":
                return new ForwardCommand(request, response);
            default:
                throw new UnsupportedCommandException(command);
        }
    }
}
