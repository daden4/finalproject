package command.factory;

import command.Command;
import command.factory.exception.UnsupportedCommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Interface for choose necessary command
 */

public interface CommandFactory {
    /**
     * return specific class for form and {@link Command}
     * @param command form name for return specific command
     * @param request user request
     * @param response response to user
     * @return necessary command for {@link Command} execute
     * @throws UnsupportedCommandException if there is not such command for request form
     */
    Command getCommand(String command, HttpServletRequest request, HttpServletResponse response) throws UnsupportedCommandException;
}
