package service.factory.impl;

import service.AdminService;
import service.GuestService;
import service.UserService;
import service.factory.ServiceFactory;
import service.impl.AdminServiceImpl;
import service.impl.GuestServiceImpl;
import service.impl.UserServiceImpl;


/**
 * Realization of {@link ServiceFactory}
 */
public class ServiceFactoryImpl implements ServiceFactory {

    private static final ServiceFactoryImpl INSTANCE = new ServiceFactoryImpl();

    private ServiceFactoryImpl() {
    }

    public static ServiceFactoryImpl getInstance() {
        return INSTANCE;
    }

    @Override
    public AdminService getAdminService() {
        return AdminServiceImpl.getInstance();
    }

    @Override
    public GuestService getGuestService() {
        return GuestServiceImpl.getInstance();
    }

    @Override
    public UserService getUserService() {
        return UserServiceImpl.getInstance();
    }

}
