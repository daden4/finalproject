package service.factory;

import service.AdminService;
import service.GuestService;
import service.UserService;

/**
 * Interface for get specific Service
 */

public interface ServiceFactory {
    /**
     * Return implementation of AdminService
     * @return concrete {@link AdminService}
     */
    AdminService getAdminService();

    /**
     * Return implementation of GuestService
     * @return concrete {@link GuestService}
     */
    GuestService getGuestService();

    /**
     * Return implementation of UserService
     * @return concrete {@link UserService}
     */
    UserService getUserService();
}
