package service;

import entity.BlockReason;
import entity.Client;
import service.exception.AdminServiceException;

import java.util.ArrayList;

/**
 * Interface of service for processing admin request
 */
public interface AdminService {
    /**
     * Get all information from selected table
     * @param productSelect is not null is it was products request
     * @param clientSelect is not null is it was clients request
     * @param orderSelect is not null is it was orders request
     * @param reasonSelect is not null is it was block reasons request
     * @return admin required information
     */
    ArrayList getRequiredList(String productSelect, String clientSelect, String orderSelect, String reasonSelect);


    /**
     * Method allow admin set ban for users ( set user[0] ban blockID[0])
     * @param blockID is array of block reasons id
     * @param userID is array of users id
     * @return updated users table with new blocks
     */
    ArrayList<Client> setBan(String[] blockID, String[] userID)  ;

    /**
     * Allow admin create it own block reasons
     * @param reasonName set name for new reason
     * @param reasonDescription set description for new reason
     * @return updated reasons set
     */

    ArrayList<BlockReason> setNewReason(String reasonName, String reasonDescription) ;
}
