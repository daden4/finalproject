package service;

import dao.exception.SameMailException;
import dao.exception.SamePhoneException;
import entity.Client;
import entity.Order;
import entity.Product;
import service.exception.InsertProductException;
import service.exception.UpdateException;
import service.exception.UserServiceException;

/**
 * Interface of service for processing user request
 */
public interface UserService {
    /**
     * Method for process change phone request
     * @param client identify client for change data
     * @param change_phone is value of new phone
     * @throws UserServiceException if data wasn't valid
     * @throws UpdateException if there is some problem with database and data wasn't changed
     * @throws SameMailException if mail already employed (doesn't throw from this type)
     * @throws SamePhoneException if phone already employed
     */
    void changePhone(Client client, String change_phone) throws UserServiceException, UpdateException, SameMailException, SamePhoneException;


    /**
     * Method for process change mail request
     * @param client identify client for change data
     * @param change_mail is value of new phone mail
     * @throws UserServiceException if data wasn't valid
     * @throws UpdateException if there is some problem with database and data wasn't changed
     * @throws SameMailException if mail already employed
     * @throws SamePhoneException if phone already employed (doesn't throw from this type)
     */
    void changeMail(Client client, String change_mail) throws UserServiceException, UpdateException, SameMailException, SamePhoneException;

    /**
     * Method for process change password request
     * @param client identify client for change data
     * @param change_password is value of new password mail
     * @throws UserServiceException if data wasn't valid
     * @throws UpdateException if there is some problem with database and data wasn't changed
     * @throws SameMailException if mail already employed (doesn't throw from this type)
     * @throws SamePhoneException if phone already employed (doesn't throw from this type)
     */

    void changePassword(Client client, String change_password) throws UserServiceException, SameMailException, SamePhoneException, UpdateException;


    /**
     * Used with shopping cart for get all information about product
     * @param productId is id of product in it database
     * @param productType is type of product in it database
     * @return initialized {@link Product}
     * @throws InsertProductException when request data have some problem
     */
    Product addProduct(String productId, String productType) throws InsertProductException;

    /**
     * Method for insert order in database
     * @param orderList is list of products which user select
     * @param orderPrice is total price of order
     * @param client is {@link Client} which made order
     * @return {@link Order} with initialized fields
     */
    Order fixOrder(String orderList, String orderPrice, Client client);
}
