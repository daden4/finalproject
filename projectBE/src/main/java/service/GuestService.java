package service;

import dao.exception.BannedPersonException;
import dao.exception.SameMailException;
import dao.exception.SameNameException;
import dao.exception.SamePhoneException;
import service.exception.GuestServiceException;
import service.exception.InvalidLogInException;
import service.exception.RegisterException;

/**
 * Interface of service for processing guest request
 */
public interface GuestService {

    /**
     * Method allow user to register
     * @param name is new user name
     * @param email is new user email
     * @param phone is new user phone
     * @param password is new user password
     * @throws SameMailException if this email already employed
     * @throws SamePhoneException if this phone already employed
     * @throws SameNameException if this name already employed
     * @throws GuestServiceException if some data isn't valid
     * @throws RegisterException if register wasn't success because of
     * some database error
     */
    void signUp(String name, String email, String phone, String password)
            throws SameMailException, SamePhoneException, SameNameException, GuestServiceException, RegisterException;

    /**
     * Process logIn request
     * @param log_username user login
     * @param log_password user password
     * @throws BannedPersonException if user have ban
     * @throws InvalidLogInException if user enter incorrect information
     */

    void signIn(String log_username, String log_password) throws BannedPersonException, InvalidLogInException;
}
