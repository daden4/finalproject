package service.impl;

import dao.ClientDAO;
import dao.exception.BannedPersonException;
import dao.exception.SameMailException;
import dao.exception.SameNameException;
import dao.exception.SamePhoneException;
import dao.factory.impl.DAOFactoryImpl;
import entity.Client;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.GuestService;
import service.exception.GuestServiceException;
import service.exception.InvalidLogInException;
import service.exception.RegisterException;
import utility.PasswordEncrypter;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Realization of {@link GuestService}
 */
public class GuestServiceImpl implements GuestService {
    private static final Logger logger = LogManager.getLogger(GuestServiceImpl.class);
    private static final GuestServiceImpl INSTANCE = new GuestServiceImpl();
    private static final String PHONE_REGEX = "^(\\+)?(375)\\d{9}$";
    private static final String USERNAME_REGEX = "^([A-Za-z])[\\w+]{4,}";
    private static final String PASSWORD_REGEX = "(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])[A-Za-z0-9]{6,}";
    private static final String EMAIL_REGEX = "^.+@.+\\..+$";
    private final static PasswordEncrypter passwordEncrypter = new PasswordEncrypter();
    private final static String KEY = "password";

    private GuestServiceImpl() {
    }

    public static GuestServiceImpl getInstance() {
        return INSTANCE;
    }

    @Override
    public void signUp(String name, String email, String phone, String password)
            throws SameMailException, SamePhoneException, SameNameException, GuestServiceException {
        if (!isValidEmail(email)) {
            logger.debug("Invalid mail format \"" + email + "\"");
            throw new GuestServiceException("Некорректная почта!");
        }
        if (!isValidUsername(name)) {
            logger.debug("Invalid login format \"" + name + "\"");
            throw new GuestServiceException("Некорректный логин!");
        }
        if (!isValidPhone(phone)) {
            logger.debug("Invalid phone format \"" + phone + "\"");
            throw new GuestServiceException("Некорректный телефон!");
        }
        if (!isValidPassword(password)) {
            logger.debug("Invalid password format \"" + password + "\"");
            throw new GuestServiceException("Некорректный пароль!");
        }



        try {
            Client client = new Client();
            client.setEmail(email);
            client.setName(name);
            client.setPhone(phone);
            client.setPassword(passwordEncrypter.encrypt(KEY, password));
            //        client.setPassword(password);

            Date date = new Date();
            SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
            client.setOrderDate(dt.format(date));

            ClientDAO dao = DAOFactoryImpl.getInstance().getClientDAO();
            if (!dao.signUp(client)){
                logger.error("Registration failed!");
                throw new RegisterException("Ошибка при регистрации.");
            }
        } catch (NoSuchAlgorithmException | InvalidKeySpecException
                | NoSuchPaddingException | InvalidKeyException |
                InvalidAlgorithmParameterException | BadPaddingException |
                UnsupportedEncodingException | IllegalBlockSizeException e) {
            logger.error(e);
        }
    }

    @Override
    public void signIn(String log_username, String log_password) throws BannedPersonException, InvalidLogInException {
        try {
            Client client = new Client();
            client.setName(log_username);
            client.setPassword(passwordEncrypter.encrypt(KEY, log_password));
//            client.setPassword(log_password);
            ClientDAO dao = DAOFactoryImpl.getInstance().getClientDAO();
            if (!dao.signIn(client)){
                logger.error("LogIn failed");
                throw new InvalidLogInException();
            }
        } catch (NoSuchAlgorithmException | InvalidKeySpecException
                | NoSuchPaddingException | InvalidKeyException
                | InvalidAlgorithmParameterException | IllegalBlockSizeException
                | BadPaddingException | IOException e) {
            e.printStackTrace();
        }
    }

    private boolean isValidPhone(String phone) {
        return isNullOrEmpty(phone) && phone.matches(PHONE_REGEX);
    }

    private boolean isValidUsername(String username) {
        return isNullOrEmpty(username) && username.matches(USERNAME_REGEX);
    }
    private boolean isValidPassword(String password) {
        return isNullOrEmpty(password) && password.matches(PASSWORD_REGEX);
    }

    private boolean isValidEmail(String email) {
        return isNullOrEmpty(email) && email.matches(EMAIL_REGEX);
    }
    private boolean isNullOrEmpty(String string) {
        return (!(string == null)) && (!string.trim().isEmpty());
    }
}
