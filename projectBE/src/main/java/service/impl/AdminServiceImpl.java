package service.impl;

import dao.BlockDAO;
import dao.ClientDAO;
import dao.OrderDAO;
import dao.ProductDAO;
import dao.factory.impl.DAOFactoryImpl;
import entity.BlockReason;
import entity.Client;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.AdminService;
import service.exception.AdminServiceException;

import java.util.ArrayList;

/**
 * Realization of {@link AdminService}
 */
public class AdminServiceImpl implements AdminService {
    private static final Logger logger = LogManager.getLogger(AdminServiceImpl.class);
    private static final AdminServiceImpl INSTANCE = new AdminServiceImpl();
    private static final String ID_REGEX = "[0-9]+";

    private AdminServiceImpl() {
    }

    public static AdminServiceImpl getInstance() {
        return INSTANCE;
    }

    @Override
    public ArrayList getRequiredList(String productSelect, String clientSelect, String orderSelect, String reasonSelect) {
        if (productSelect != null){
            ProductDAO productDAO = DAOFactoryImpl.getInstance().getProductDAO();
            return  productDAO.getProductList();
        } else if(clientSelect != null){
            ClientDAO clientDAO = DAOFactoryImpl.getInstance().getClientDAO();
            return clientDAO.getClientList() ;
        } else if(orderSelect != null){
            OrderDAO orderDAO = DAOFactoryImpl.getInstance().getOrderDAO();
            return orderDAO.getOrderList();
        } else if(reasonSelect != null){
            BlockDAO blockDAO = DAOFactoryImpl.getInstance().getBlockDAO();
            return blockDAO.getBlockReasonList();
        }
        return new ArrayList();
    }

    @Override
    public ArrayList<Client> setBan(String[] blockID, String[] userID) {
        ClientDAO clientDAO = DAOFactoryImpl.getInstance().getClientDAO();
        try {
            checkBlockId(blockID);
            clientDAO.updateBan(blockID, userID);
            return clientDAO.getClientList();
        } catch (AdminServiceException e) {
            logger.error(e);
        }
        return clientDAO.getClientList();
    }

    private void checkBlockId(String[] blockID) throws AdminServiceException {
        for (String id : blockID){
            if (!(isValidBlockID(id))){
                logger.debug("Invalid block ID format!");
                throw new AdminServiceException();
            }
        }
    }


    @Override
    public ArrayList<BlockReason> setNewReason(String reasonName, String reasonDescription)   {
        BlockDAO blockDAO = DAOFactoryImpl.getInstance().getBlockDAO();
        try {
            if (isNullOrEmpty(reasonName) || isNullOrEmpty(reasonDescription)){
                throw new AdminServiceException();
            }
            BlockReason blockReason = new BlockReason();
            blockReason.setName(reasonName);
            blockReason.setDescription(reasonDescription);
            blockDAO.insertNewReason(blockReason);
            return blockDAO.getBlockReasonList();
        } catch (AdminServiceException e){
            logger.info("Bad reason (xss)", e);
        }
        return blockDAO.getBlockReasonList();

    }
    private boolean isValidBlockID(String id) {
        return isEmpty(id) || id.matches(ID_REGEX);
    }
    private boolean isEmpty(String string) {
        return string.trim().isEmpty();
    }
    private boolean isNullOrEmpty(String string) {
        return (string == null) || (string.trim().isEmpty());
    }
}
