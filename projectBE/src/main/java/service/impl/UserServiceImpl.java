package service.impl;

import dao.ClientDAO;
import dao.OrderDAO;
import dao.ProductDAO;
import dao.exception.SameMailException;
import dao.exception.SamePhoneException;
import dao.factory.impl.DAOFactoryImpl;
import entity.Client;
import entity.Order;
import entity.Product;
import entity.ProductType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.UserService;
import service.exception.InsertProductException;
import service.exception.UpdateException;
import service.exception.UserServiceException;
import utility.OrderProductInitializer;
import utility.PasswordEncrypter;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Realization of {@link UserService}
 */
public class UserServiceImpl implements UserService {
    private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);
    private static final UserServiceImpl INSTANCE = new UserServiceImpl();
    private static final String PHONE_REGEX = "^(\\+)?(375)\\d{9}$";
    private static final String EMAIL_REGEX = "^.+@.+\\..+$";
    private static final String PASSWORD_REGEX = "(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])[A-Za-z0-9]{6,}";
    private final static PasswordEncrypter passwordEncrypter = new PasswordEncrypter();
    private final static String KEY = "password";

    private UserServiceImpl() {
    }

    public static UserServiceImpl getInstance() {
        return INSTANCE;
    }

    @Override
    public void changePhone(Client client, String change_phone) throws UserServiceException, UpdateException, SameMailException, SamePhoneException {
        if (!isValidPhone(change_phone)) {
            logger.debug("Invalid new phone format \"" + change_phone + "\"");
            throw new UserServiceException("Некорректный телефон!");
        }
        ClientDAO clientDAO = DAOFactoryImpl.getInstance().getClientDAO();
        client.setPhone(change_phone);
        if (!clientDAO.changeData(client,"phone")){
            logger.error("Phone wasn't change");
            throw new UpdateException("Данные не были изменены");
        }
    }

    @Override
    public void changeMail(Client client, String change_mail) throws UserServiceException, UpdateException, SameMailException, SamePhoneException {
        if (!isValidEmail(change_mail)) {
            logger.debug("Invalid new mail format \"" + change_mail + "\"");
            throw new UserServiceException("Некорректая почта!");
        }
        ClientDAO clientDAO = DAOFactoryImpl.getInstance().getClientDAO();
        client.setEmail(change_mail);
        if (!clientDAO.changeData(client,"email")){
            logger.error("Email wasn't change");
            throw new UpdateException("Данные не были изменены");
        }
    }

    @Override
    public void changePassword(Client client, String change_password) throws UserServiceException, SameMailException, SamePhoneException, UpdateException {
        if (!isValidPassword(change_password)) {
            logger.debug("Invalid new password format \"" + change_password + "\"");
            throw new UserServiceException("Некорректный пароль!");
        }
        try {
            ClientDAO clientDAO = DAOFactoryImpl.getInstance().getClientDAO();
            client.setPassword(passwordEncrypter.encrypt(KEY, change_password));
//            client.setPassword(change_password);
            if (!clientDAO.changeData(client,"password")){
                logger.error("Password wasn't change");
                throw new UpdateException("Данные не были изменены");
            }
        } catch (NoSuchAlgorithmException | InvalidKeySpecException
                | NoSuchPaddingException | InvalidKeyException |
                InvalidAlgorithmParameterException | BadPaddingException |
                UnsupportedEncodingException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }

    }

    @Override
    public Product addProduct(String productId, String productType) throws InsertProductException {
        if (!(isNullOrEmpty(productId)||isNullOrEmpty(productType))){
            logger.error("Empty ID or type of product at insert.");
            throw new InsertProductException();
        }
        Product product = new Product();
        ProductDAO productDAO = DAOFactoryImpl.getInstance().getProductDAO();
        product.setProductID(Integer.parseInt(productId));
        product.setProductType(getProductType(productType));
        productDAO.initializeProduct(product);
        return product;
    }

    @Override
    public Order fixOrder(String orderList, String orderPrice, Client client) {
        Order order = new Order();
        OrderProductInitializer orderProductInitializer = new OrderProductInitializer();
        orderProductInitializer.initProduct(orderList, order);
        order.setOrderPrice(orderPrice);
        order.setClient(client);

        Date date = new Date();
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
        order.setOrderDate(dt.format(date));

        OrderDAO orderDAO = DAOFactoryImpl.getInstance().getOrderDAO();
        orderDAO.addNewOrder(order);
        return order;
    }

    private ProductType getProductType(String product) throws InsertProductException {
        ProductType productType;
        switch (product){
            case "pizza":
                productType = ProductType.PIZZA;
                break;
            case "salad":
                productType = ProductType.SALAD;
                break;
            case "drink":
                productType = ProductType.DRINK;
                break;
            default:
                logger.error("No such product type.");
                throw new InsertProductException();
        }
        return productType;
    }


    private boolean isValidPhone(String phone) {
        return isNullOrEmpty(phone) && phone.matches(PHONE_REGEX);
    }
    private boolean isValidEmail(String email) {
        return isNullOrEmpty(email) && email.matches(EMAIL_REGEX);
    }
    private boolean isNullOrEmpty(String string) {
        return (!(string == null)) && (!string.trim().isEmpty());
    }
    private boolean isValidPassword(String password) {
        return isNullOrEmpty(password) && password.matches(PASSWORD_REGEX);
    }

}
