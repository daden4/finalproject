package service.exception;

/**
 * Exception throw on {@link service.UserService} when can't initialize some product
 */
public class InsertProductException extends UserServiceException {
    public InsertProductException() {
        super();
    }

    public InsertProductException(String message) {
        super(message);
    }

    public InsertProductException(String message, Throwable cause) {
        super(message, cause);
    }

    public InsertProductException(Throwable cause) {
        super(cause);
    }

    protected InsertProductException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
