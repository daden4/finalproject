package service.exception;

/**
 * Exception throw on some problem in {@link service.GuestService}
 */
public class GuestServiceException extends ServiceException {
    public GuestServiceException() {
        super();
    }

    public GuestServiceException(String message) {
        super(message);
    }

    public GuestServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public GuestServiceException(Throwable cause) {
        super(cause);
    }

    protected GuestServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
