package service.exception;

/**
 * Exception throw on {@link service.GuestService} when user enter invalid login
 */
public class InvalidLogInException extends GuestServiceException {
    public InvalidLogInException() {
        super();
    }

    public InvalidLogInException(String message) {
        super(message);
    }

    public InvalidLogInException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidLogInException(Throwable cause) {
        super(cause);
    }

    protected InvalidLogInException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
