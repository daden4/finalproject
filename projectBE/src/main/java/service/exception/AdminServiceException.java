package service.exception;

/**
 * Exception throw on some problem in {@link service.AdminService}
 */
public class AdminServiceException extends ServiceException {
    public AdminServiceException() {
        super();
    }

    public AdminServiceException(String message) {
        super(message);
    }

    public AdminServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public AdminServiceException(Throwable cause) {
        super(cause);
    }

    protected AdminServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
