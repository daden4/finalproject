package utility;

import dao.factory.impl.DAOFactoryImpl;
import entity.Client;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Utility for initialize order when language change
 */
public class LanguageSessionInitializer {
    public void initialize(HttpServletRequest request, HttpServletResponse response){
        String locale = (String) request.getSession().getAttribute("locale");
        Client client = (Client) request.getSession().getAttribute("client");
        if (client != null){
            OrderHistoryParser orderHistoryParser = new OrderHistoryParser();
            String updateOrderList =
                    orderHistoryParser.parse(DAOFactoryImpl.getInstance().getOrderDAO().getOrderList(),client.getName(), locale);
            request.getSession().setAttribute("order",updateOrderList);
        }
    }
}
