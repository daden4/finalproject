package utility;

import entity.Client;
import entity.UserRole;

/**
 * Utility for initialize {@link Client}
 */
class ClientParser {
    Client parse(String allUserInformation) {
        String[] userInfo = allUserInformation.split(" ");
        Client client = new Client();
        client.setName(userInfo[0]);
        client.setPhone(userInfo[1]);
        client.setEmail(userInfo[2]);
        client.setDiscount(Integer.parseInt(userInfo[3]));
        client.setOrderDate(userInfo[4]);
        client.setId(Integer.parseInt(userInfo[5]));
        client.setStatus(UserRole.valueOf(userInfo[6].toUpperCase()));
        return client;
    }
}
