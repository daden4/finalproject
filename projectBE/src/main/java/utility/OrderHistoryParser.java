package utility;

import entity.Order;
import entity.Product;

import java.util.ArrayList;

/**
 * Utility for return formalize order history
 * for user
 */

public class OrderHistoryParser {
    String parse(ArrayList<Order> orders, String log_username, String locale) {
        String resultString ="";
        switch (locale){
            case "ru":
                resultString = ruParse(orders, log_username);
                break;
            case "en":
                resultString = enParse(orders, log_username);
                break;
        }
     return resultString;
    }

    private String enParse(ArrayList<Order> orders, String log_username) {
        StringBuilder stringBuilder = new StringBuilder();

        for (Order order: orders){
            if (order.getClient().getName().equals(log_username)) {
                stringBuilder.append("<p>");

                stringBuilder.append("Order ID: ").append(order.getOrderId());

                for (Product product : order.getProductList()) {
                    if (product.getEnName() != null){
                        stringBuilder.append(" ").append(product.getEnName())
                                .append(" quantity ").append(product.getQuantity())
                                .append("pcs., price: ").append(product.getPrice()).append(" р");
                    }
                }

                stringBuilder.append(". Order date:  ").append(order.getOrderDate());
                stringBuilder.append(". Price of the order with discount:  ").append(order.getOrderPrice()).append(" р.");


                stringBuilder.append("</p>");
            }

        }
        return stringBuilder.toString();
    }

    private String ruParse(ArrayList<Order> orders, String log_username) {
        StringBuilder stringBuilder = new StringBuilder();

        for (Order order: orders){
            if (order.getClient().getName().equals(log_username)) {
                stringBuilder.append("<p>");

                stringBuilder.append("Идентификатор заказа: ").append(order.getOrderId());

                for (Product product : order.getProductList()) {
                    if (product.getProductName() != null){
                        stringBuilder.append(" ").append(product.getProductName())
                                .append(" количеством ").append(product.getQuantity())
                                .append("шт., цена: ").append(product.getPrice()).append(" р");
                    }
                }

                stringBuilder.append(". Дата заказа:  ").append(order.getOrderDate());
                stringBuilder.append(". Цена заказа с учетом скидки:  ").append(order.getOrderPrice()).append(" р.");


                stringBuilder.append("</p>");
            }

        }
        return stringBuilder.toString();
    }
}
