package utility;

import entity.Order;
import entity.Product;
import entity.ProductType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Utility for ordering products from shopping cart
 */
public class OrderProductInitializer {
    public void initProduct(String orderList, Order order) {
        ArrayList <Product> productList  = order.getProductList();
        List<String> orderProducts = Arrays.asList(orderList.split(" "));

        Map<String ,Long > map = orderProducts.stream()
                .collect( Collectors.groupingBy(Function.identity(),Collectors.counting())) ;

        map.forEach((key, value) -> {
            Product product = new Product();
            product.setProductID(Integer.parseInt(key.replaceAll("\\D+","")));
            product.setProductType(ProductType.valueOf(key.replaceAll("[^a-z]+","").toUpperCase()));
            product.setQuantity(value);
            productList.add(product);
        });

    }
}
