package utility;

import dao.factory.impl.DAOFactoryImpl;
import entity.Client;
import entity.Product;
import entity.UserRole;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Utility for initialize HttpSession on user LogIn
 */
public class LogInSessionInitializer {
    private static final Logger logger = LogManager.getLogger(LogInSessionInitializer.class);
    public void initialize(HttpServletRequest request, HttpServletResponse response) {
        String log_username = request.getParameter("log_username");
        try {
            HttpSession session = request.getSession();
            ClientParser clientParser = new ClientParser();
            Client client = clientParser.parse(DAOFactoryImpl.getInstance().
                    getClientDAO().getAllUserInformation(log_username));
            if (client.getStatus().equals(UserRole.ADMIN)) {
                response.getWriter().println("/WEB-INF/jsp/admin/admin.jsp");
                session.setAttribute("admin", client);
            } else {
                response.getWriter().println("index.jsp");
                String locale = (String) request.getSession().getAttribute("locale");
                OrderHistoryParser orderHistoryParser = new OrderHistoryParser();
                String orderList = orderHistoryParser.parse(DAOFactoryImpl.getInstance().
                        getOrderDAO().getOrderList(), log_username, locale);
                session.setAttribute("login", true);
                session.setAttribute("client", client);
                session.setAttribute("order", orderList);
                session.setAttribute("ajaxOrder", new ArrayList<Product>());
                session.setAttribute("orderPrice", "0.00");
            }
        } catch (IOException e){
            logger.error(e);
        }
    }
}
