package utility;

import dao.factory.impl.DAOFactoryImpl;
import entity.Client;
import entity.Order;
import entity.Product;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;


/**
 * Utility for update session {@link Order} on it change
 */
public class OrderFixSessionInitializer {
    public void initialize(HttpServletRequest request, HttpServletResponse response){
        String locale = (String) request.getSession().getAttribute("locale");
        Order order = (Order) request.getAttribute("orderRequest");
        OrderHistoryParser orderHistoryParser = new OrderHistoryParser();
        Client client = order.getClient();
        String updateOrderList =
                orderHistoryParser.parse(DAOFactoryImpl.getInstance().getOrderDAO().getOrderList(),client.getName(), locale);
        request.getSession().setAttribute("order",updateOrderList);
        request.getSession().setAttribute("client",client);
        request.getSession().setAttribute("ajaxOrder",new ArrayList<Product>());
        request.getSession().setAttribute("orderPrice","0.00");
    }
}
