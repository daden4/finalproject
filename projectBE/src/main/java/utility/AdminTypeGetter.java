package utility;

/**
 * Utility for identify button type
 * and set it in admin.jsp
 */
public class AdminTypeGetter {
    public String getType(String productSelect, String clientSelect, String orderSelect, String reasonSelect){
        if (productSelect != null){
            return  "product";
        } else if(clientSelect != null){
            return "client" ;
        } else if(orderSelect != null){
            return "order";
        } else if(reasonSelect != null){
            return "blockreason";
        }
        return "";
    }
}
