package utility;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Utility which set response status for
 *process it in ajax method
 */
public class AjaxResponseSetter {
    private static final Logger logger = LogManager.getLogger(AjaxResponseSetter.class);

    public void setResponse(HttpServletResponse response, Exception e, int status){
        try {
            response.setStatus(status);
            response.setContentType("text/html; charset=UTF-8");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(e.getMessage());
            response.flushBuffer();
        } catch (IOException ex) {
            logger.error(ex);
        }
    }
    public void setSimpleResponse(HttpServletResponse response, int status){
        try {
            response.setStatus(status);
            response.flushBuffer();
        } catch (IOException ex) {
            logger.error(ex);
        }
    }

    public void setPositiveResponse(HttpServletResponse response, String message, int status) {
        try {
            response.setStatus(status);
            response.setContentType("text/html; charset=UTF-8");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(message);
            response.flushBuffer();
        } catch (IOException e) {
            logger.error(e);
        }
    }
}
