package utility;

import entity.Product;

/**
 * Utility to return necessary product info
 * in according to language.
 */
public class ProductInfoHandler {
    public String initialize(Product product, String lang){
        String productName;
        switch (lang){
            case "ru":
                productName = product.getProductName();
                break;
            case "en":
                productName = product.getEnName();
                break;
            default:
                productName = "";
        }
        return productName + " " + product.getSize() + " " + product.getPrice();
    }
}
