package dao.impl;

import dao.ClientDAO;
import dao.OrderDAO;
import dao.ProductDAO;
import dao.factory.DAOFactory;
import dao.factory.impl.DAOFactoryImpl;
import dao.pool.ConnectionPool;
import dao.pool.exception.PoolException;
import entity.Client;
import entity.Order;
import entity.Product;
import entity.ProductType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;

/**
 * Realization of {@link OrderDAO}
 */
public class OrderDAOImpl implements OrderDAO {
    private static final Logger logger = LogManager.getLogger(OrderDAOImpl.class);

    private final static String GET_ALL_ORDERS = "SELECT o.order_id, o.order_date, c.name,\n" +
            "       p.name, op.pizza_quantity, p.price, p.en_name, \n" +
            "       s.name, op.salad_quantity, s.price, s.en_name, \n" +
            "       d.name, op.drink_quantity, d.price, d.en_name, \n" +
            "       o.price\n" +
            "FROM `order`o\n" +
            "LEFT JOIN client c on o.client_id = c.client_id\n" +
            "LEFT JOIN order_products op on o.order_id = op.order_id\n" +
            "LEFT JOIN salad s on op.salad_id = s.salad_id\n" +
            "LEFT JOIN drink d on op.drink_id = d.drink_id\n" +
            "LEFT JOIN pizza p on op.pizza_id = p.pizza_id;";

    private final static String ADD_ORDER ="INSERT INTO `order` (client_id, order_date, price) VALUES (?, ?, ?)";

    private final static String LAST_ID = "SELECT order_id FROM `order` ORDER BY order_id DESC LIMIT 1";


    private static final OrderDAOImpl INSTANCE = new OrderDAOImpl();

    private OrderDAOImpl() {
    }

    public static OrderDAOImpl getInstance() {
        return INSTANCE;
    }
    private  ConnectionPool pool = ConnectionPool.getInstance();

    @Override
    public boolean addNewOrder(Order order) {
        Connection connection = null;
        PreparedStatement statement = null;
        Statement lastIdSt = null;
        ResultSet lastIdRs = null;
        try {
            connection = pool.getConnection();
            statement = connection
                    .prepareStatement(ADD_ORDER);
            statement.setString(1, String.valueOf(order.getClient().getId()));
            statement.setString(2, order.getOrderDate());
            statement.setString(3, order.getOrderPrice());
            int successValue = statement.executeUpdate();

            if (successValue != 0) {
                lastIdSt = connection.createStatement();
                lastIdRs = lastIdSt.executeQuery(LAST_ID);
                if (lastIdRs.next()) {
                    int orderId = lastIdRs.getInt(1);
                    order.setOrderId(orderId);
                }
                DAOFactory daoFactory = DAOFactoryImpl.getInstance();
                ClientDAO clientDAO = daoFactory.getClientDAO();
                clientDAO.onOrderDataChange(order);

                ProductDAO productDAO = daoFactory.getProductDAO();
                if (!productDAO.addProductList(order)) {
                    return false;
                }
            }
            return successValue != 0;
        } catch (SQLException  e) {
            logger.error(e);
        } catch (PoolException e) {
            logger.error("Pool is over!", e);
        } finally {
            pool.closeConnection(connection);
            pool.closeResultSet(lastIdRs);
            pool.closePreparedStatement(statement);
            pool.closeStatement(lastIdSt);
        }
        return false;
    }

    @Override
    public ArrayList<Order> getOrderList() {
        ArrayList<Order> orders = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = pool.getConnection();
            statement = connection
                    .createStatement();
            resultSet = statement.executeQuery(GET_ALL_ORDERS);
            Order order = null;
            int prevOrderID = 0;
            while (resultSet.next()) {
                int currentOrderID = resultSet.getInt(1);
                if (prevOrderID != currentOrderID) {
                    order = new Order();
                }
                order.setOrderId(currentOrderID);
                order.setOrderDate(resultSet.getString(2));
                order.setOrderPrice(resultSet.getString(16));
                order.setClient(new Client(resultSet.getString(3)));

                ArrayList<Product> productList = order.getProductList();
                productList.add(new Product(ProductType.PIZZA, resultSet.getLong(5),
                        resultSet.getString(4), resultSet.getString(6),
                        resultSet.getString(7)));
                productList.add(new Product(ProductType.SALAD, resultSet.getLong(9),
                        resultSet.getString(8), resultSet.getString(10),
                        resultSet.getString(11)));
                productList.add(new Product(ProductType.DRINK, resultSet.getLong(13),
                        resultSet.getString(12), resultSet.getString(14),
                        resultSet.getString(15)));

                if (prevOrderID != currentOrderID) {
                    prevOrderID = currentOrderID;
                    orders.add(order);
                }
            }

        } catch (SQLException e) {
            logger.error(e);
        } catch (PoolException e) {
            logger.error("Pool is over!", e);
        } finally {
            pool.closeAll(resultSet, statement, connection);
        }
        return orders;
    }
}
