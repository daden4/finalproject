package dao.impl;

import dao.ClientDAO;
import dao.exception.BannedPersonException;
import dao.exception.SameMailException;
import dao.exception.SameNameException;
import dao.exception.SamePhoneException;
import dao.pool.ConnectionPool;
import dao.pool.exception.PoolException;
import entity.Client;
import entity.Order;
import entity.UserRole;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;

/**
 * Realization of {@link ClientDAO}
 */
public class ClientDAOImpl  implements ClientDAO {
    private static final Logger logger = LogManager.getLogger(ClientDAOImpl.class);
    private final static String SIGN_IN = "SELECT name FROM client WHERE name=? AND password=?";
    private final static String SIGN_UP = "INSERT INTO client"
            + " (name, phone, email, password, order_date)"
            + " VALUES(?,?,?,?,?)";
    private final static String SAME_EMAIL = "SELECT email FROM client where email=?";
    private final static String SAME_PHONE = "SELECT  phone FROM client WHERE phone=?";
    private final static String SAME_NAME = "SELECT  name FROM client WHERE name=?";
    private final static String IS_BANNED = "SELECT ban FROM client WHERE name = ?";
    private final static String CHANGE_PHONE = "UPDATE client SET phone = ? WHERE name = ?";
    private final static String CHANGE_EMAIL = "UPDATE client SET email = ? WHERE name = ?";
    private final static String CHANGE_PASSWORD = "UPDATE client SET password = ? WHERE name = ?";
    private final static String BAN_REASON = "SELECT c.name, c.phone, b.name, b.description " +
            "FROM client c INNER JOIN block_reasons b ON c.ban = b.reason_id AND c.name = ?;";
    private final static String ALL_INFO = "SELECT name, phone, email, discount, order_date, client_id, client_type FROM client WHERE name = ?;";
    private final static String ON_CREATE_ORDER_UPDATE = "UPDATE client SET order_date = ?, discount = discount + 1 WHERE client_id = ?;";
    private final static String ALL_USERS = "SELECT * FROM client;";
    private final static String UPDATE_BAN = "UPDATE client SET ban = ? WHERE client_id = ?";


    private static final ClientDAOImpl INSTANCE = new ClientDAOImpl();

    private ClientDAOImpl() {
    }

    public static ClientDAOImpl getInstance() {
        return INSTANCE;
    }
    private  ConnectionPool pool = ConnectionPool.getInstance();


    @Override
    public boolean signIn(Client client) throws BannedPersonException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = pool.getConnection();
            boolean notBannedPerson
                    = isNotBanned(connection, client.getName());
            if (notBannedPerson) {
                statement = connection
                        .prepareStatement(SIGN_IN);
                statement.setString(1, client.getName());
                statement.setString(2, client.getPassword());
                resultSet = statement.executeQuery();
                return resultSet.next();
            }
        } catch (SQLException  e) {
            logger.error(e);
        } catch (PoolException e) {
            logger.error("Pool is over.", e);
        } finally {
            pool.closeAll(resultSet, statement, connection);
        }
        return false;
    }


    @Override
    public boolean signUp(Client client) throws SameMailException, SamePhoneException, SameNameException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = pool.getConnection();
            boolean notSameEmail
                    = isNotSameEmail(connection, client.getEmail());
            boolean notSamePhone
                    = isNotSamePhone(connection, client.getPhone());
            boolean notSameName
                    = isNotSameName(connection, client.getName());

            if (notSameEmail && notSamePhone && notSameName) {
                statement = connection.prepareStatement(SIGN_UP);
                statement.setString(1, client.getName());
                statement.setString(2, client.getPhone());
                statement.setString(3, client.getEmail());
                statement.setString(4, client.getPassword());
                statement.setString(5, client.getOrderDate());

                statement.executeUpdate();
                return true;
            }

        } catch (SQLException e) {
            logger.error(e);
        } catch (PoolException e) {
            logger.error("Pool is over.", e);
        } finally {
            pool.closeConnection(connection);
            pool.closePreparedStatement(statement);
        }
        return false;
    }

    @Override
    public void onOrderDataChange(Order order) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = pool.getConnection();
            statement = connection
                    .prepareStatement(ON_CREATE_ORDER_UPDATE);
            Client client = order.getClient();
            statement.setString(1, order.getOrderDate());
            statement.setString(2, String.valueOf(client.getId()));
            client.setDiscount(client.getDiscount() + 1);
            client.setOrderDate(order.getOrderDate());
            int successValue = statement.executeUpdate();

        } catch (SQLException  e) {
            logger.error(e);
        } catch (PoolException e) {
            logger.error("Pool is over.", e);
        } finally {
            pool.closeConnection(connection);
            pool.closePreparedStatement(statement);
        }
    }


    @Override
    public boolean changeData(Client client, String parameter) throws SamePhoneException, SameMailException {
        String statement ="";
        String newValue = "";
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = pool.getConnection();
            switch (parameter) {
                case "phone":
                    statement = CHANGE_PHONE;
                    newValue = client.getPhone();
                    isNotSamePhone(connection, newValue);
                    break;
                case "email":
                    statement = CHANGE_EMAIL;
                    newValue = client.getEmail();
                    isNotSameEmail(connection, newValue);
                    break;
                case "password":
                    statement = CHANGE_PASSWORD;
                    newValue = client.getPassword();
                    break;
            }


            preparedStatement = connection
                    .prepareStatement(statement);
            preparedStatement.setString(1, newValue);
            preparedStatement.setString(2, client.getName());
            preparedStatement.executeUpdate();
            return true;

        } catch (SQLException e) {
            logger.error(e);
        } catch (PoolException e) {
            logger.error("Pool is over.", e);
        } finally {
            pool.closeConnection(connection);
            pool.closePreparedStatement(preparedStatement);
        }
        return false;
    }

    @Override
    public String getAllUserInformation(String name) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = pool.getConnection();
            statement = connection
                    .prepareStatement(ALL_INFO);
            statement.setString(1, name);
            resultSet = statement.executeQuery();
            String result = "";
            while (resultSet.next()) {
                result = resultSet.getString(1) + " " + resultSet.getString(2) +
                        " " + resultSet.getString(3) + " " + resultSet.getInt(4) +
                        " " + resultSet.getString(5) + " " + resultSet.getString(6)
                        + " " + resultSet.getString(7);
            }
            return result;

        } catch (SQLException  e) {
            logger.error(e);
        } catch (PoolException e) {
            logger.error("Pool is over.", e);
        } finally {
            pool.closeAll(resultSet, statement, connection);
        }
        return "";
    }


    @Override
    public ArrayList<Client> getClientList() {
        ArrayList<Client> clients = new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        Statement statement = null;
        try {
            connection = pool.getConnection();
            statement = connection
                    .createStatement();
            resultSet = statement.executeQuery(ALL_USERS);
            while (resultSet.next()) {
                Client client = new Client();
                client.setId(resultSet.getInt(1));
                client.setName(resultSet.getString(2));
                client.setPhone(resultSet.getString(3));
                client.setEmail(resultSet.getString(4));
                client.setPassword(resultSet.getString(5));
                client.setOrderDate(resultSet.getString(6));
                client.setDiscount(resultSet.getInt(7));
                client.setBanReason(resultSet.getString(8));
                client.setStatus(UserRole.valueOf(resultSet.getString(9).toUpperCase()));
                clients.add(client);
            }
        } catch (SQLException  e) {
            logger.error(e);
        } catch (PoolException e) {
            logger.error("Pool is over.", e);
        } finally {
            pool.closeAll(resultSet, statement, connection);
        }
        return clients;
    }

    @Override
    public void updateBan(String[] blockID, String[] userID) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = pool.getConnection();
            statement = connection.prepareStatement(UPDATE_BAN);
            for (int i = 0; i < blockID.length; i++) {
                String block = (blockID[i].equals("")) ? null : blockID[i];
                statement.setString(1, block);
                statement.setString(2, userID[i]);
                statement.executeUpdate();
            }
        } catch (SQLException  e) {
            logger.error(e);
        } catch (PoolException e) {
            logger.error("Pool is over.", e);
        } finally {
            pool.closeConnection(connection);
            pool.closePreparedStatement(statement);
        }
    }

    private boolean isNotSameEmail(Connection connection, String email)
            throws SQLException, SameMailException {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(SAME_EMAIL);
            statement.setString(1, email);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                logger.info("Same mail.");
                throw new SameMailException("Этот email уже зарегистрирован.");
            } else {
                return true;
            }
        } finally {
            pool.closePreparedStatement(statement);
            pool.closeResultSet(resultSet);
        }

    }

    private boolean isNotSamePhone(Connection connection, String phone)
            throws SQLException, SamePhoneException {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(SAME_PHONE);
            statement.setString(1, phone);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                logger.info("Same phone.");
                throw new SamePhoneException("Этот телефон уже зарегистрирован.");
            } else {
                return true;
            }
        } finally {
            pool.closeResultSet(resultSet);
            pool.closePreparedStatement(statement);
        }

    }

    private boolean isNotSameName(Connection connection, String name)
            throws SQLException, SameNameException {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(SAME_NAME);
            statement.setString(1, name);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                logger.info("Same login.");
                throw new SameNameException("Логин занят.");
            } else {
                return true;
            }
        } finally {
            pool.closePreparedStatement(statement);
            pool.closeResultSet(resultSet);
        }

    }

    private boolean isNotBanned (Connection connection, String name) throws SQLException, BannedPersonException {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(IS_BANNED);
            statement.setString(1, name);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                int reason = resultSet.getInt(1);
                if (reason != 0) {
                    PreparedStatement reasonStatement = connection.prepareStatement(BAN_REASON);
                    reasonStatement.setString(1, name);
                    ResultSet reasonSet = reasonStatement.executeQuery();
                    String reasonDesc ="";
                    while (reasonSet.next()) {
                        reasonDesc = "\nИмя пользователя: " + reasonSet.getString(1) + ", телефон: " +
                                reasonSet.getString(2) + ".\nПричина блокировки: " +
                                reasonSet.getString(3) + ".";
                    }
                    logger.warn("Banned person");
                    throw new BannedPersonException("Этот пользователь заблокирован! " + reasonDesc);
                } else {
                    return true;
                }
            } else {
                return true;
            }
        } finally {
            pool.closePreparedStatement(statement);
            pool.closeResultSet(resultSet);
        }

    }

}
