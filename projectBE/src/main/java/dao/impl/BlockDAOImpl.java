package dao.impl;

import dao.BlockDAO;
import dao.pool.ConnectionPool;
import dao.pool.exception.PoolException;
import entity.BlockReason;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;

/**
 * Realization of {@link BlockDAO}
 */

public class BlockDAOImpl implements BlockDAO {
    private static final Logger logger = LogManager.getLogger(BlockDAOImpl.class);
    private static final String ALL_BLOCK_REASONS = "SELECT * FROM block_reasons;";
    private static final String INSERT_REASON = "INSERT INTO block_reasons (name, description) VALUES (?,?);";

    private static final BlockDAOImpl INSTANCE = new BlockDAOImpl();

    private BlockDAOImpl() {
    }

    public static BlockDAOImpl getInstance() {
        return INSTANCE;
    }

    private  ConnectionPool pool = ConnectionPool.getInstance();

    @Override
    public ArrayList<BlockReason> getBlockReasonList() {
        ArrayList<BlockReason> blockReasons = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = pool.getConnection();
            statement = connection
                    .createStatement();
            resultSet = statement.executeQuery(ALL_BLOCK_REASONS);
            while (resultSet.next()) {
                BlockReason blockReason = new BlockReason();
                blockReason.setId(resultSet.getInt(1));
                blockReason.setName(resultSet.getString(2));
                blockReason.setDescription(resultSet.getString(3));
                blockReasons.add(blockReason);
            }

        } catch (SQLException e) {
            logger.error(e);
        } catch (PoolException e) {
            logger.error("Pool is over.", e);
        } finally {
            pool.closeAll(resultSet, statement, connection);
        }
        return blockReasons;
    }

    @Override
    public boolean insertNewReason(BlockReason blockReason) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = pool.getConnection();
            statement = connection
                    .prepareStatement(INSERT_REASON);
            statement.setString(1, blockReason.getName());
            statement.setString(2, blockReason.getDescription());
            int successValue = statement.executeUpdate();
            return successValue != 0;
        } catch (SQLException e) {
            logger.error(e);
        } catch (PoolException e) {
            logger.error("Pool is over.", e);
        } finally {
            pool.closeConnection(connection);
            pool.closePreparedStatement(statement);
        }
        return false;
    }
}
