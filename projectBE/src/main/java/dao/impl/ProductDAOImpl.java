package dao.impl;

import dao.ProductDAO;
import dao.pool.ConnectionPool;
import dao.pool.exception.PoolException;
import entity.Order;
import entity.Product;
import entity.ProductType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;

/**
 * Realization of {@link ProductDAO}
 */

public class ProductDAOImpl implements ProductDAO {
    private static final Logger logger = LogManager.getLogger(ProductDAOImpl.class);
    private static final String ADD_PRODUCT = "INSERT INTO order_products" +
            " (order_id, pizza_id, pizza_quantity, salad_id, salad_quantity, drink_id, drink_quantity) " +
            "VALUES (?,?,?,?,?,?,?)";
    private final String GET_ALL_PRODUCTS = "SELECT * FROM pizza\n" +
            "UNION\n" +
            "SELECT * FROM drink\n" +
            "UNION\n" +
            "SELECT * FROM salad;";


    private static final ProductDAOImpl INSTANCE = new ProductDAOImpl();

    private ProductDAOImpl() {
    }

    public static ProductDAOImpl getInstance() {
        return INSTANCE;
    }
    private  ConnectionPool pool = ConnectionPool.getInstance();

    @Override
    public boolean addProductList(Order order) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = pool.getConnection();
            statement = connection
                    .prepareStatement(ADD_PRODUCT);
            int successValue = 0;

            ArrayList<Product> productList = order.getProductList();

            while (productList.size() != 0) {
                int usePizzaId = -1;
                int useSaladId = -1;
                int useDrinkId = -1;
                statement.setString(1, String.valueOf(order.getOrderId()));
                statement.setString(2, null);
                statement.setString(3, null);
                statement.setString(4, null);
                statement.setString(5, null);
                statement.setString(6, null);
                statement.setString(7, null);

                for (int i = 0; i < productList.size(); i++) {
                    Product product = productList.get(i);
                    if (product.getProductType().equals(ProductType.PIZZA) && usePizzaId == -1) {
                        statement.setString(2, String.valueOf(product.getProductID()));
                        statement.setString(3, String.valueOf(product.getQuantity()));
                        usePizzaId = i;
                    } else if (product.getProductType().equals(ProductType.SALAD) && useSaladId == -1) {
                        statement.setString(4, String.valueOf(product.getProductID()));
                        statement.setString(5, String.valueOf(product.getQuantity()));
                        useSaladId = i;
                    } else if (product.getProductType().equals(ProductType.DRINK) && useDrinkId == -1) {
                        statement.setString(6, String.valueOf(product.getProductID()));
                        statement.setString(7, String.valueOf(product.getQuantity()));
                        useDrinkId = i;
                    }
                }
                if (usePizzaId > -1) {
                    productList.remove(usePizzaId);
                    if (useDrinkId > 0) {
                        useDrinkId -= 1;
                    }
                    if (useSaladId > 0) {
                        useSaladId -= 1;
                    }
                }
                if (useDrinkId > -1) {
                    productList.remove(useDrinkId);
                    if (useSaladId > 0) {
                        useSaladId -= 1;
                    }
                }
                if (useSaladId > -1) {
                    productList.remove(useSaladId);
                }

                successValue = statement.executeUpdate();
            }
            return successValue != 0;
        } catch (SQLException e) {
            logger.error(e);
        } catch (PoolException e) {
            logger.error("Pool is over.", e);
        } finally {
            pool.closeConnection(connection);
            pool.closePreparedStatement(statement);
        }
        return false;
    }

    @Override
    public void initializeProduct(Product product) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = pool.getConnection();
            String dbName = product.getProductType().toString().toLowerCase();
            String state = findStatement(dbName);
            statement
                    = connection.prepareStatement(state);
            statement.setInt(1, product.getProductID());
            resultSet = statement.executeQuery();

            while (resultSet.next()) {

                product.setProductName(resultSet.getString(2));
                product.setSize(resultSet.getString(3));
                product.setPrice(resultSet.getString(4));
                product.setEnName(resultSet.getString(5));
            }

        } catch (SQLException e) {
            logger.error(e);
        } catch (PoolException e) {
            logger.error("Pool is over.", e);
        } finally {
            pool.closeAll(resultSet, statement, connection);
        }

    }

    @Override
    public ArrayList<Product> getProductList() {
        ArrayList<Product> products = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = pool.getConnection();
            statement
                    = connection.createStatement();
            resultSet = statement.executeQuery(GET_ALL_PRODUCTS);

            ProductType[] productTypeArr = ProductType.values();
            int productTypeCount = 0;
            ProductType currentType = null;
            while (resultSet.next()) {
                if (resultSet.getInt(1) == 1) {
                    currentType = productTypeArr[productTypeCount];
                    productTypeCount++;
                }
                Product product = new Product();
                product.setProductID(resultSet.getInt(1));
                product.setProductName(resultSet.getString(2));
                product.setSize(resultSet.getString(3));
                product.setPrice(resultSet.getString(4));
                product.setEnName(resultSet.getString(5));
                product.setProductType(currentType);
                products.add(product);
            }
        } catch (SQLException e) {
            logger.error(e);
        } catch (PoolException e) {
            logger.error("Pool is over.", e);
        } finally {
            pool.closeAll(resultSet, statement, connection);
        }
        return products;
    }

    private String findStatement(String dbName) {
        String FIND_BY_DRINK_ID = "SELECT * FROM drink WHERE drink_id = ?";
        String FIND_BY_SALAD_ID = "SELECT * FROM salad WHERE salad_id = ?";
        String FIND_BY_PIZZA_ID = "SELECT * FROM pizza WHERE pizza_id = ?";
        switch (dbName) {
            case "drink":
                return FIND_BY_DRINK_ID;
            case "salad":
                return FIND_BY_SALAD_ID;
            case "pizza":
                return FIND_BY_PIZZA_ID;
        }
        return dbName;
    }


}
