package dao.pool;


import dao.pool.exception.PoolException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.Enumeration;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *Database connection pool
 * use tomcat context.xml
 */
public class ConnectionPool {
    private static ConnectionPool instance;

    private static final Logger logger = LogManager.getLogger(ConnectionPool.class);

    /**
     *Get ConnectionPool
     * @return thread safe ConnectionPool
     */
    public static ConnectionPool getInstance() {
        Lock locker = new ReentrantLock();
        locker.lock();
        try {
            if (instance == null) {
                instance = new ConnectionPool();
            }
        } finally {
            locker.unlock();
        }
        return instance;
    }

    /**
     *Get Connection
     * @return connection to database
     * @throws PoolException when can't get connection
     */
    public Connection getConnection() throws PoolException {
        Context context;
        try {
            context = (Context) new InitialContext().lookup("java:comp/env");
            DataSource dataSource = (DataSource) context.lookup("jdbc/pizzeria");
            return dataSource.getConnection();
        } catch (NamingException | SQLException e) {
            logger.error(e);
            throw new PoolException("Connection timeout is over.");
        }

    }

    /**
     * Close Connection
     * @param currentConnection will close
     */
    public void closeConnection(Connection currentConnection) {
        try {
            if (currentConnection != null) {
                currentConnection.close();
            }
        } catch (SQLException e) {
            logger.error(e);
        }

    }

    /**
     * Close PreparedStatement
     * @param statement will close
     */

    public void closePreparedStatement(PreparedStatement statement) {
        try {
            if (statement != null) {
                statement.close();
            }
        } catch (SQLException e) {
            logger.error(e);
        }
    }

    /**
     * Close ResultSet
     * @param resultSet will close
     */
    public void closeResultSet(ResultSet resultSet) {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
        } catch (SQLException e) {
            logger.error(e);
        }
    }

    /**
     * close statement
     * @param statement will close
     */
    public void closeStatement(Statement statement) {
        try {
            if (statement != null) {
                statement.close();
            }
        } catch (SQLException e) {
            logger.error(e);
        }
    }



    public void closeAll(ResultSet resultSet, PreparedStatement statement, Connection connection){
        closeResultSet(resultSet);
        closePreparedStatement(statement);
        closeConnection(connection);
    }
    public void closeAll(ResultSet resultSet, Statement statement, Connection connection){
        closeResultSet(resultSet);
        closeStatement(statement);
        closeConnection(connection);
    }


}