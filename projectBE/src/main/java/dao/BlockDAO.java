package dao;

import entity.BlockReason;

import java.util.ArrayList;

/**
 *Interface which provide work with block_reasons table
 */
public interface BlockDAO {
    /**
     * Select all block reasons which can be assigned to user by admin
     * @return list of block reasons
     */
    ArrayList<BlockReason> getBlockReasonList();

    /**
     * Allow admin create it own block reasons
     * @param blockReason will add into table
     * @return true when block reason was added
     */
    boolean insertNewReason(BlockReason blockReason);
}
