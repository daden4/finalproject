package dao;

import dao.exception.BannedPersonException;
import dao.exception.SameMailException;
import dao.exception.SameNameException;
import dao.exception.SamePhoneException;
import entity.Client;
import entity.Order;

import java.util.ArrayList;

/**
 *Interface which provide work with client table
 */
public interface ClientDAO {
    /**
     * Use for find user in database
     * @param client will search into table
     * @return true if user is exist
     * @throws BannedPersonException if user has ban
     */
    boolean signIn(Client client) throws BannedPersonException;

    /**
     *Add new user into database
     * @param client will add into table
     * @return true if registration success
     * @throws SameMailException if email is employed
     * @throws SamePhoneException if phone is employed
     * @throws SameNameException if name is employed
     */
    boolean signUp(Client client) throws SameMailException, SamePhoneException, SameNameException;


    /**
     * Update client information from order (discount and order date)
     * @param order will update
     */
    void onOrderDataChange(Order order);

    /**
     *Method for change user information in account
     * @param client will change
     * @param parameter set which parameter will change
     * @return true if change was success
     * @throws SamePhoneException if phone is employed
     * @throws SameMailException if mail is employed
     */
    boolean changeData(Client client, String parameter) throws SamePhoneException, SameMailException;

    /**
     * Return all information of user "name"
     * @param name will search in table
     * @return all information
     */
    String getAllUserInformation(String name);

    /**
     * Use for get all register users
     * @return all client
     */
    ArrayList<Client> getClientList();

    /**
     * Update all users blocks ( set user[0] block blockID[0])
     * @param blockID array of block reasons id
     * @param userID array of users id
     */
    void updateBan(String[] blockID, String[] userID);
}
