package dao;

import entity.Order;

import java.util.ArrayList;
/**
 *Interface which provide work with order table
 */
public interface OrderDAO {
    /**
     * Work with {@link ProductDAO} for fix user order
     * @param order will add into table
     * @return true if order was added successful
     */
    boolean addNewOrder(Order order);

    /**
     * Select all orders from database
     * @return all orders
     */
    ArrayList<Order> getOrderList();
}
