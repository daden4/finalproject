package dao.factory;

import dao.BlockDAO;
import dao.ClientDAO;
import dao.OrderDAO;
import dao.ProductDAO;

/**
 * Interface for get specific DAO
 */
public interface DAOFactory {
    /**
     *Return implementation of BlockDAO
     * @return concrete {@link BlockDAO}
     */
    BlockDAO getBlockDAO();

    /**
     *Return implementation of ClientDAO
     * @return concrete {@link ClientDAO}
     */
    ClientDAO getClientDAO();

    /**
     *Return implementation of OrderDAO
     * @return concrete {@link OrderDAO}
     */
    OrderDAO getOrderDAO();

    /**
     *Return implementation of ProductDAO
     * @return concrete {@link ProductDAO}
     */

    ProductDAO getProductDAO();


}