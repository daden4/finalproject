package dao.factory.impl;

import dao.BlockDAO;
import dao.ClientDAO;
import dao.OrderDAO;
import dao.ProductDAO;
import dao.factory.DAOFactory;
import dao.impl.BlockDAOImpl;
import dao.impl.ClientDAOImpl;
import dao.impl.OrderDAOImpl;
import dao.impl.ProductDAOImpl;

/**
 * Realization of {@link DAOFactory}
 */
public class DAOFactoryImpl implements DAOFactory {
    private static final DAOFactoryImpl INSTANCE = new DAOFactoryImpl();

    private DAOFactoryImpl(){}

    public static DAOFactoryImpl getInstance(){
        return INSTANCE;
    }

    @Override
    public BlockDAO getBlockDAO() {
        return BlockDAOImpl.getInstance();
    }

    @Override
    public ClientDAO getClientDAO() {
        return ClientDAOImpl.getInstance();
    }

    @Override
    public OrderDAO getOrderDAO() {
        return OrderDAOImpl.getInstance();
    }

    @Override
    public ProductDAO getProductDAO() {
        return ProductDAOImpl.getInstance();
    }
}
