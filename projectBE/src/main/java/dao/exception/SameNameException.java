package dao.exception;
/**
 * Throw when person try register same name
 */
public class SameNameException extends Exception{
    public SameNameException() {
        super();
    }

    public SameNameException(String message) {
        super(message);
    }

    public SameNameException(String message, Throwable cause) {
        super(message, cause);
    }

    public SameNameException(Throwable cause) {
        super(cause);
    }

    protected SameNameException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
