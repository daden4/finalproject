package dao.exception;

/**
 * Throw when banned person try logIn
 */
public class BannedPersonException extends Exception {
    public BannedPersonException() {
        super();
    }

    public BannedPersonException(String message) {
        super(message);
    }

    public BannedPersonException(String message, Throwable cause) {
        super(message, cause);
    }

    public BannedPersonException(Throwable cause) {
        super(cause);
    }

    protected BannedPersonException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

