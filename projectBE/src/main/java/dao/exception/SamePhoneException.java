package dao.exception;
/**
 * Throw when person try register same phone
 */
public class SamePhoneException extends Exception{
    public SamePhoneException() {
        super();
    }

    public SamePhoneException(String message) {
        super(message);
    }

    public SamePhoneException(String message, Throwable cause) {
        super(message, cause);
    }

    public SamePhoneException(Throwable cause) {
        super(cause);
    }

    protected SamePhoneException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
