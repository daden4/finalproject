package dao.exception;

/**
 * Throw when person try register same mail
 */
public class SameMailException extends Exception {
    public SameMailException() {
        super();
    }

    public SameMailException(String message) {
        super(message);
    }

    public SameMailException(String message, Throwable cause) {
        super(message, cause);
    }

    public SameMailException(Throwable cause) {
        super(cause);
    }

    protected SameMailException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
