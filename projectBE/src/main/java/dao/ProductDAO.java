package dao;

import entity.Order;
import entity.Product;

import java.util.ArrayList;
/**
 *Interface which provide work with product table
 */
public interface ProductDAO {
    /**
     * Method for division user order and add it into table.
     * Work with {@link OrderDAO}
     * @param order will insert into it product list into order_products table
     * @return true if order was insert success
     */
    boolean addProductList(Order order);

    /**
     * Set product fields to it values from database
     * @param product will search into database
     *
     */
    void initializeProduct(Product product);

    /**
     * Method for get all products in menu
     * @return initialized products
     */
    ArrayList<Product> getProductList();
}
