package controller;

import command.Command;
import command.exception.CommandException;
import command.factory.CommandFactory;
import command.factory.exception.UnsupportedCommandException;
import command.factory.impl.CommandFactoryImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 *Main controller of web application.
 */

@WebServlet(value = "/controller", name = "controller")
public class Controller extends javax.servlet.http.HttpServlet {

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) {
        doPost(request,response);
    }

    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response)  {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) {
        String jspCommand = request.getParameter("form");
        CommandFactory factory = CommandFactoryImpl.getInstance();
        executeCommand(factory, jspCommand, request, response);
    }


    private void executeCommand(CommandFactory factory, String commandName, HttpServletRequest request, HttpServletResponse response) {
        Logger logger = LogManager.getLogger(Controller.class);
        try {
            if (commandName != null) {
                Command command = factory.getCommand(commandName, request, response);
                command.execute();
            }
        } catch (CommandException e) {
            logger.error(e);
        } catch (UnsupportedCommandException e) {
            logger.error("No such command", e);
        }
    }
}